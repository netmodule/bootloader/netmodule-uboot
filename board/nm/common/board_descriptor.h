/*
 * Board descriptor library
 *
 * (c) COPYRIGHT 2010-2019 by NetModule AG, Switzerland.
 *
 * SPDX-License-Identifier:	GPL-2.0+
 */

#ifndef __BOARD_DESCRIPTOR_H
#define __BOARD_DESCRIPTOR_H

#include "bdparser.h"		/* BD_Context */


void bd_register_context_list(const BD_Context *list, size_t count);
int bd_get_context(BD_Context *bdctx, uint32_t i2caddress, uint32_t offset);

int bd_get_prodname(char *prodname, size_t len);
int bd_get_variantname(char *variantname, size_t len);
void bd_get_hw_type(int* type);
void bd_get_hw_version(int* ver, int* rev);
void bd_get_hw_patch(int* patch);
int bd_get_mac(int index, uint8_t *macaddr, size_t len);
uint32_t bd_get_fpgainfo(void);
int bd_get_pd_dio(char *config, size_t len);
int bd_get_pd_serial(char *config, size_t len);
int bd_get_pd_module(uint32_t slot, char *config, size_t len);
int bd_get_sim_config(char* simconfig, size_t len);
int bd_get_devicetree(char* devicetreename, size_t len);
int bd_get_shield(uint32_t shieldnr);
uint8_t bd_get_boot_partition(void);

#endif	/* __BOARD_DESCRIPTOR_H */
