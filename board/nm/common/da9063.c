/*
 * da9063.c
 *
 * Dialog DA9063 PMIC
 *
 * Copyright (C) 2018-2024 NetModule AG - http://www.netmodule.com/
 *
 * SPDX-License-Identifier:	GPL-2.0+
 */

#include <common.h>
#include <errno.h>
#include <i2c.h>

#include "da9063.h"


static int da9063_i2c_bus = 0;
static int bus_claimed = 0;


static int switch_i2c_bus(void)
{
	int old_bus;

	old_bus = i2c_get_bus_num();
	if (old_bus != da9063_i2c_bus) {
		i2c_set_bus_num(da9063_i2c_bus);
	}

	bus_claimed++;

	return old_bus;
}

static void revert_i2c_bus(int bus)
{
	if (da9063_i2c_bus != bus) {
		i2c_set_bus_num(bus);
	}

	bus_claimed--;
}


void da9063_init(int i2c_bus)
{
	da9063_i2c_bus = i2c_bus;
}

int da9063_claim_i2c_bus(void)
{
	return switch_i2c_bus();
}

void da9063_release_i2c_bus(int bus)
{
	revert_i2c_bus(bus);
}

int da9063_get_reg(uint32_t reg, uint8_t* val)
{
	int ret;
	uint8_t temp;

	/* Argument check */
	if ((reg >= 0x200) || (val==0)) {
		return -1;
	}

	/* State check. Has bus been claimed */
	if (bus_claimed == 0) {
		return -2;
	}

	*val = 0;
	if (reg < 0x100) {
		ret = i2c_read(CONFIG_PMIC_I2C_ADDR+0, reg & 0xFF, 1, &temp, 1);
	} else {
		ret = i2c_read(CONFIG_PMIC_I2C_ADDR+1, reg & 0xFF, 1, &temp, 1);
	}

	if (ret == 0)
		*val = temp;

	return ret;
}

int da9063_set_reg(uint32_t reg, uint8_t val)
{
	int ret;

	/* Argument check */
	if (reg >= 0x200) {
		return -1;
	}

	/* State check. Has bus been claimed */
	if (bus_claimed == 0) {
		return -2;
	}

	if (reg < 0x100) {
		ret = i2c_write(CONFIG_PMIC_I2C_ADDR+0, reg & 0xFF, 1, &val, 1);
	} else {
		ret = i2c_write(CONFIG_PMIC_I2C_ADDR+1, reg & 0xFF, 1, &val, 1);
	}

	if (ret != 0)
		puts("da9063 write error\n");

	return ret;
}

void da9063_set_gpio(unsigned bit, int state)
{
	int pmic_reg;
	int ret;
	uint8_t bitmask;
	uint8_t reg = 0x00;

	if (bit <= 7) {
		pmic_reg = PMIC_REG_GPIO_MODE0_7;
		bitmask = 1U << (bit-0);
	}
	else  {
		pmic_reg = PMIC_REG_GPIO_MODE8_15;
		bitmask = 1U << (bit-8);
	}

	ret = da9063_get_reg(pmic_reg, &reg);

	if (ret == 0)  {
		if (state)	reg |=  bitmask;
		else		reg &= ~bitmask;

		(void)da9063_set_reg(pmic_reg, reg);
	}
}

int da9063_get_gpio(unsigned bit, int* state)
{
	int pmic_reg;
	int ret;
	uint8_t bitmask;
	uint8_t reg = 0x00;

	*state = 0;

	if (bit <= 7) {
		pmic_reg = PMIC_REG_GPIO_MODE0_7;
		bitmask = 1U << (bit-0);
	}
	else  {
		pmic_reg = PMIC_REG_GPIO_MODE8_15;
		bitmask = 1U << (bit-8);
	}

	ret = da9063_get_reg(pmic_reg, &reg);
	if (ret == 0)  {
		*state = ((reg & bitmask) != 0);
	}

	return ret;
}
