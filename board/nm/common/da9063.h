/*
 * da9063.c
 *
 * Dialog DA9063 PMIC
 *
 * Copyright (C) 2018-2024 NetModule AG - http://www.netmodule.com/
 *
 * SPDX-License-Identifier:	GPL-2.0+
 */

#ifndef DA9063_H
#define DA9063_H


#define CONFIG_PMIC_I2C_BUS	0
#define CONFIG_PMIC_I2C_ADDR	0x58	/* Pages 0 and 1, Pages 2 and 3 -> 0x59 */

#define PMIC_REG_STATUS_A		0x01	/* Status of ON_KEY, WAKE, COMP1V2, DVC */
#define PMIC_REG_STATUS_A_COMP1V2_MASK	0x08

#define PMIC_REG_FAULT_LOG		0x05	/* PMIC fault log register, holding reset reason */
#define PMIC_FAULT_TWD_ERROR_MASK	0x01	/* Watchdog timeout detected */
#define PMIC_FAULT_POR_MASK		0x02	/* Startup from No-Power/RTC/Delivery mode */

#define PMIC_REG_EVENT_A		0x06
#define PMIC_REG_EVENT_ONKEY_MASK	0x01
#define PMIC_REG_EVENT_RTC_ALARM_MASK	0x02
#define PMIC_REG_EVENT_RTC_TICK_MASK	0x04
#define PMIC_REG_EVENT_EVENTS_B_MASK	0x20

#define PMIC_REG_EVENT_B		0x07
#define PMIC_REG_EVENT_COMP1V2_MASK	0x04

#define PMIC_REG_IRQ_MASK_A	0x0A
#define PMIC_REG_IRQ_MASK_B	0x0B
#define PMIC_REG_IRQ_MASK_C	0x0C
#define PMIC_REG_IRQ_MASK_D	0x0D

#define PMIC_REG_CONTROL_A      0x0E    /* Control register for power states */
#define PMIC_REG_CONTROL_D	0x11    /* Control register for blink/watchdog */
#define PMIC_REG_GPIO14_15	0x1C    /* Configuration of GPIO14/15 (mode, wake) */
#define PMIC_REG_GPIO_MODE0_7	0x1D	/* Control register for GPIOs 0..7 */
#define PMIC_REG_GPIO_MODE8_15	0x1E	/* Control register for GPIOs 8..15 */

#define PMIC_REG_BCORE1_CONT	0x21	/* Control register of BCORE1 */
#define PMIC_REG_BCORE2_CONT	0x20	/* Control register of BCORE2 */
#define PMIC_REG_BPERI_CONT	0x25	/* Control register of BPERI */
#define PMIC_REG_BIO_CONT	0x24	/* Control register of BIO */
#define PMIC_REG_BMEM_CONT	0x23	/* Control register of BMEM */

#define PMIC_REG_LDO3_CONT	0x28	/* Control register of LDO3 */
#define PMIC_REG_LDO6_CONT	0x2B	/* Control register of LDO6 */
#define PMIC_REG_LDO7_CONT	0x2C	/* Control register of LDO7 */
#define PMIC_REG_LDO11_CONT	0x30	/* Control register of LDO11 */

#define PMIC_LDOx_EN_MASK	0x01
#define PMIC_LDOx_CONF_MASK	0x80

#define PMIC_REG_ID_4_3		0x84
#define PMIC_REG_ID_6_5		0x85

#define PMIC_REG_BUCK_ILIM_A	0x9A
#define PMIC_REG_BUCK_ILIM_B	0x9B
#define PMIC_REG_BUCK_ILIM_C	0x9C

#define PMIC_REG_BCORE1_CONF	0x9E	/* Configuration register of BCORE1 */
#define PMIC_REG_BCORE2_CONF	0x9D	/* Configuration register of BCORE2 */
#define PMIC_REG_BPERI_CONF	0xA2	/* Configuration register of BPERI */
#define PMIC_REG_BIO_CONF	0xA0	/* Configuration register of BIO */
#define PMIC_REG_BMEM_CONF	0xA1	/* Configuration register of BMEM */

#define PMIC_CONF_MODE_MASK	0xC0
#define PMIC_CONF_MODE_SLEEP	0x40
#define PMIC_CONF_MODE_SYNC	0x80
#define PMIC_CONF_MODE_AUTO	0xC0


#define PMIC_REG_BBAT_CONT	0xC5	/* Control register for backup battery */

#define PMIC_REG_CONFIG_E	0x10A
#define PMIC_REG_CONFIG_G	0x10C
#define PMIC_REG_CONFIG_L	0x111

#define PMIC_REG_TRIM_CLDR	0x120	/* Calendar Trim register, 2's complement, 1.9ppm per bit */

#define PMIC_GP_ID_0		0x121	/* General purpose ID 0 (R/W) */
#define PMIC_GP_ID_1		0x122	/* General purpose ID 1 (R/W) */

#define PMIC_REG_CONFIG_ID	0x184	/* OTP Config ID <ver.rev> */


extern void da9063_init(int i2c_bus);

extern int da9063_claim_i2c_bus(void);
extern void da9063_release_i2c_bus(int bus);

extern int da9063_get_reg(uint32_t reg, uint8_t* val);
extern int da9063_set_reg(uint32_t reg, uint8_t val);

extern void da9063_set_gpio(unsigned bit, int state);
extern int da9063_get_gpio(unsigned bit, int* state);


#endif /* DA9063_H */
