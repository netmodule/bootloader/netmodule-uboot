/*
 * ether_crc.h
 *
 * Ethernet CRC computation
 *
 * Copyright (C) 2018-2020 NetModule AG - http://www.netmodule.com/
 *
 * SPDX-License-Identifier:	GPL-2.0+
 */

#ifndef ETHER_CRC_H
#define ETHER_CRC_H


extern uint32_t ether_crc(size_t len, uint8_t const *p);


#endif /* ETHER_CRC_H */
