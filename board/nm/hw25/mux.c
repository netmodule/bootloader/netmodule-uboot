/*
 * mux.c
 *
 * Copyright (C) 2018-2019 NetModule AG - http://www.netmodule.com/
 * Copyright (C) 2011 Texas Instruments Incorporated - http://www.ti.com/
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation version 2.
 *
 * This program is distributed "as is" WITHOUT ANY WARRANTY of any
 * kind, whether express or implied; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 */

#include <common.h>
#include <asm/arch/sys_proto.h>
#include <asm/arch/hardware.h>
#include <asm/arch/mux.h>
#include <asm/io.h>
#include "board.h"

static struct module_pin_mux gpio_pin_mux[] = {
	/*
	* CPU GPIOs
	*
	* (J18) GPIO0_16: RST_PHY~
	* (U10) GPIO0_22: SEL_RS232/RS485~
	* (T10) GPIO0_23: EN_RS485_TERM~
	* (T11) GPIO0_26: IO_OUT1
	* (U12) GPIO0_27: IO_OUT2
	*
	* (T12) GPIO1_12: IO_IN0
	* (T13) GPIO1_13: IO_IN1
	* (T14) GPIO1_14: IO_IN2
	* (T15) GPIO1_15: IO_IN3
	*
	* (T13) GPIO2_0: RST_SDCARD~
	* (L17) GPIO2_18: GSM_PWR_EN
	* (L16) GPIO2_19: RST_GSM
	*
	* (K18) GPIO3_9: WLAN_IRQ
	* (L18) GPIO3_10: WLAN_EN
	* (C12) GPIO3_17: SIM_SEL
	*/

	/* Bank 0 */
	{OFFSET(mii1_txd3), (MODE(7) | PULLUDDIS)},				/* (J18) gpio0[16] */  /* RST_PHY~ */
	{OFFSET(gpmc_ad8), (MODE(7) | PULLUDDIS)},				/* (U10) gpio0[22] */  /* SEL_RS232/RS485~ */
	{OFFSET(gpmc_ad9), (MODE(7) | PULLUDDIS)},				/* (T10) gpio0[23] */  /* EN_RS485_TERM~ */
	{OFFSET(gpmc_ad10), (MODE(7) | PULLUDDIS)},				/* (T11) gpio0[26] */  /* IO_OUT1 */
	{OFFSET(gpmc_ad11), (MODE(7) | PULLUDDIS)},				/* (U12) gpio0[27] */  /* IO_OUT2 */

	/* Bank 1 */
	{OFFSET(gpmc_ad12), (MODE(7) | PULLUDDIS | RXACTIVE)},			/* (T12) gpio1[12] */  /* IO_IN0 */
	{OFFSET(gpmc_ad13), (MODE(7) | PULLUDDIS | RXACTIVE)},			/* (R12) gpio1[13] */  /* IO_IN1 */
	{OFFSET(gpmc_ad14), (MODE(7) | PULLUDDIS | RXACTIVE)},			/* (V13) gpio1[14] */  /* IO_IN2 */
	{OFFSET(gpmc_ad15), (MODE(7) | PULLUDDIS | RXACTIVE)},			/* (U13) gpio1[15] */  /* IO_IN3 */

	/* TODO: What about all the unused GPMC pins ? */

	/* Bank 2 */
	{OFFSET(gpmc_be1n), (MODE(7) | PULLUDDIS)},				/* (T13) gpio2[0] */  /* RST_SDCARD~ */
	{OFFSET(mii1_rxd3), (MODE(7) | PULLUDDIS)},				/* (L17) gpio2[18] */  /* GSM_PWR_EN */
	{OFFSET(mii1_rxd2),  (MODE(7) | PULLUDDIS)},				/* (L16) gpio2[19] */  /* RST_GSM */


#if 0
	/* TODO: What is this meant for? */
	{OFFSET(lcd_data3), (MODE(7) | PULLUDEN | PULLUP_EN)},	/* (R4) gpio2[9] */  /* SYSBOOT_3 */
	{OFFSET(lcd_data4), (MODE(7) | PULLUDEN | PULLUP_EN)},	/* (T1) gpio2[10] */  /* SYSBOOT_4 */

	/* TODO: Check other unued pins from sysboot block */
	/* Ensure PU/PD does not work against external signal */
	/*
	 * SYSBOOT 0,1,5,12,13 = Low
	 * SYSBOOT 2 = High
	 */
#endif

	/* Bank 3 */
	{OFFSET(mii1_txclk),  (MODE(7) | PULLUDDIS | RXACTIVE)},		/* (K18) gpio3[9] */  /* WLAN_IRQ */
	{OFFSET(mii1_rxclk), (MODE(7) | PULLUDDIS)},				/* (L18) gpio3[10] */  /* WLAN_EN */
	{OFFSET(mcasp0_ahclkr), (MODE(7) | PULLUDEN | PULLDOWN_EN)},		/* (C12) gpio3[17] */  /* SIM_SEL */
	{-1}
};

/* I2C0 PMIC */
static struct module_pin_mux i2c0_pin_mux[] = {
	{OFFSET(i2c0_sda), (MODE(0) | RXACTIVE | PULLUDEN | PULLUP_EN | SLEWCTRL)},	/* (C17) I2C0_SDA */
	{OFFSET(i2c0_scl), (MODE(0) | RXACTIVE | PULLUDEN | PULLUP_EN | SLEWCTRL)},	/* (C16) I2C0_SCL */
	{-1}
};

/* I2C2 System */
static struct module_pin_mux i2c2_pin_mux[] = {
	{OFFSET(uart1_rtsn), (MODE(3) | RXACTIVE | PULLUDEN | PULLUP_EN | SLEWCTRL)},	/* (D17) I2C2_SCL */
	{OFFSET(uart1_ctsn), (MODE(3) | RXACTIVE | PULLUDEN | PULLUP_EN | SLEWCTRL)},	/* (D18) I2C2_SDA */
	{-1},
};

/* RMII1: Ethernet */
static struct module_pin_mux rmii1_pin_mux[] = {
	/* RMII */
	{OFFSET(mii1_crs), MODE(1) | PULLUDDIS | RXACTIVE},			/* (H17) rmii1_crs */
	{OFFSET(mii1_rxerr), MODE(7) | PULLUDEN | PULLDOWN_EN | RXACTIVE},	/* (J15) gpio (rxerr) */
	{OFFSET(mii1_rxd0), MODE(1) | PULLUDDIS | RXACTIVE},			/* (M16) rmii1_rxd0 */
	{OFFSET(mii1_rxd1), MODE(1) | PULLUDDIS | RXACTIVE},			/* (L15) rmii1_rxd1 */
	{OFFSET(mii1_txen), MODE(1) | PULLUDDIS},				/* (J16) rmii1_txen */
	{OFFSET(mii1_txd0), MODE(1) | PULLUDDIS},				/* (K17) rmii1_txd0 */
	{OFFSET(mii1_txd1), MODE(1) | PULLUDDIS},				/* (K16) rmii1_txd1 */
	{OFFSET(rmii1_refclk), MODE(0) | PULLUDDIS | RXACTIVE},			/* (H18) rmii1_refclk */

	/* SMI */
	{OFFSET(mdio_clk), MODE(0) | PULLUDDIS},				/* (M18) mdio_clk */
	{OFFSET(mdio_data), MODE(0) | PULLUDEN | PULLUP_EN | RXACTIVE},		/* (M17) mdio_data */

	/* 25MHz Clock Output */
	{OFFSET(xdma_event_intr0), MODE(3)},					/* (A15) clkout1 (25 MHz clk for PHY) */
	{-1}
};

/* RMII2: Ethernet */
static struct module_pin_mux rmii2_pin_mux[] = {
	/* RMII */
	{OFFSET(gpmc_a9), MODE(3) | PULLUDDIS | RXACTIVE},			/* (U16) rmii2_crs */
	{OFFSET(gpmc_wpn), MODE(7) | PULLUDEN | PULLDOWN_EN | RXACTIVE},	/* (U17) gpio (rxerr) */
	{OFFSET(gpmc_a11), MODE(3) | PULLUDDIS | RXACTIVE},			/* (V17) rmii2_rxd0 */
	{OFFSET(gpmc_a10), MODE(3) | PULLUDDIS | RXACTIVE},			/* (T16) rmii2_rxd1 */
	{OFFSET(gpmc_a0), MODE(3) | PULLUDDIS},					/* (R13) rmii2_txen */
	{OFFSET(gpmc_a5), MODE(3) | PULLUDDIS},					/* (V15) rmii2_txd0 */
	{OFFSET(gpmc_a4), MODE(3) | PULLUDDIS},					/* (R14) rmii2_txd1 */
	{OFFSET(mii1_col), MODE(1) | PULLUDDIS | RXACTIVE},			/* (H16) rmii2_refclk */
	{-1},
};

/* MMC0: WiFi */
static struct module_pin_mux mmc0_sdio_pin_mux[] = {
	{OFFSET(mmc0_clk), (MODE(0) | PULLUDEN | PULLUP_EN | RXACTIVE)},	/* (G17) MMC0_CLK */
	{OFFSET(mmc0_cmd), (MODE(0) | PULLUDEN | PULLUP_EN | RXACTIVE)},	/* (G18) MMC0_CMD */
	{OFFSET(mmc0_dat0), (MODE(0) | PULLUDEN | PULLUP_EN | RXACTIVE)},	/* (G16) MMC0_DAT0 */
	{OFFSET(mmc0_dat1), (MODE(0) | PULLUDEN | PULLUP_EN | RXACTIVE)},	/* (G15) MMC0_DAT1 */
	{OFFSET(mmc0_dat2), (MODE(0) | PULLUDEN | PULLUP_EN | RXACTIVE)},	/* (F18) MMC0_DAT2 */
	{OFFSET(mmc0_dat3), (MODE(0) | PULLUDEN | PULLUP_EN | RXACTIVE)},	/* (F17) MMC0_DAT3 */
	{-1}
};

/* MMC1: eMMC */
static struct module_pin_mux mmc1_emmc_pin_mux[] = {
	{OFFSET(gpmc_csn1), (MODE(2) | PULLUDEN | PULLUP_EN | RXACTIVE)},	/* (U9) MMC1_CLK */
	{OFFSET(gpmc_csn2), (MODE(2) | PULLUDEN | PULLUP_EN | RXACTIVE)},	/* (V9) MMC1_CMD */
	{OFFSET(gpmc_ad0), (MODE(1) | PULLUDEN | PULLUP_EN | RXACTIVE)},	/* (U7) MMC1_DAT0 */
	{OFFSET(gpmc_ad1), (MODE(1) | PULLUDEN | PULLUP_EN | RXACTIVE)},	/* (V7) MMC1_DAT1 */
	{OFFSET(gpmc_ad2), (MODE(1) | PULLUDEN | PULLUP_EN | RXACTIVE)},	/* (R8) MMC1_DAT2 */
	{OFFSET(gpmc_ad3), (MODE(1) | PULLUDEN | PULLUP_EN | RXACTIVE)},	/* (T8) MMC1_DAT3 */
	{OFFSET(gpmc_ad4), (MODE(1) | PULLUDEN | PULLUP_EN | RXACTIVE)},	/* (U8) MMC1_DAT4 */
	{OFFSET(gpmc_ad5), (MODE(1) | PULLUDEN | PULLUP_EN | RXACTIVE)},	/* (V8) MMC1_DAT5 */
	{OFFSET(gpmc_ad6), (MODE(1) | PULLUDEN | PULLUP_EN | RXACTIVE)},	/* (R9) MMC1_DAT6 */
	{OFFSET(gpmc_ad7), (MODE(1) | PULLUDEN | PULLUP_EN | RXACTIVE)},	/* (T9) MMC1_DAT7 */
	{-1}
};

/* USB_DRVBUS not used -> configure as GPIO */
static struct module_pin_mux usb_pin_mux[] = {
	{OFFSET(usb0_drvvbus), (MODE(7) | PULLUDDIS)},				/* (F16) USB0_DRVVBUS */
	{OFFSET(usb1_drvvbus), (MODE(7) | PULLUDDIS)},				/* (F15) USB1_DRVVBUS */
	{-1}
};

/* UART0: User (Debug/Console) */
static struct module_pin_mux uart0_pin_mux[] = {
	{OFFSET(uart0_rxd), (MODE(0) | PULLUDEN | PULLUP_EN | RXACTIVE)},	/* (E15) UART0_RXD */
	{OFFSET(uart0_txd), (MODE(0) | PULLUDEN | PULLUP_EN | SLEWCTRL)},	/* (E16) UART0_TXD */
	{-1},
};

/* UART5: RS232/RS485 */
/* CTS is unused - set to GPIO mode */
static struct module_pin_mux uart5_pin_mux[] = {
	{OFFSET(lcd_data9), (MODE(4) | PULLUDEN | PULLUP_EN | RXACTIVE)},	/* (U2) UART5_RXD */
	{OFFSET(lcd_data8), (MODE(4) | PULLUDEN | PULLUP_EN)},			/* (U1) UART5_TXD */
	{OFFSET(lcd_data14), (MODE(7) | PULLUDEN | PULLDOWN_EN)},			/* (V4) uart5_ctsn */
	{OFFSET(lcd_data15), (MODE(6) | PULLUDEN | PULLUP_EN)},			/* (T5) uart5_rtsn */
	{-1}
};

static struct module_pin_mux unused_pin_mux[] = {
	/* SYSBOOT6, 7, 10, 11: Not used pulldown active, receiver disabled */
	{OFFSET(lcd_data6), (MODE(7) | PULLUDEN | PULLDOWN_EN)},
	{OFFSET(lcd_data7), (MODE(7) | PULLUDEN | PULLDOWN_EN)},
	{OFFSET(lcd_data10), (MODE(7) | PULLUDEN | PULLDOWN_EN)},
	{OFFSET(lcd_data11), (MODE(7) | PULLUDEN | PULLDOWN_EN)},

	/* TODO: GPMCA1..3, A6..8 */

	{-1}
};


void enable_board_pin_mux(void)
{
	configure_module_pin_mux(gpio_pin_mux);

	configure_module_pin_mux(rmii1_pin_mux);
	configure_module_pin_mux(rmii2_pin_mux);
	configure_module_pin_mux(mmc0_sdio_pin_mux);
	configure_module_pin_mux(mmc1_emmc_pin_mux);
	configure_module_pin_mux(usb_pin_mux);

	configure_module_pin_mux(i2c0_pin_mux);
	configure_module_pin_mux(i2c2_pin_mux);

	configure_module_pin_mux(uart5_pin_mux);

	configure_module_pin_mux(unused_pin_mux);
}

void enable_uart0_pin_mux(void)
{
	configure_module_pin_mux(uart0_pin_mux);
}

