/*
 * board.h
 *
 * TI AM335x boards information header
 *
 * Copyright (C) 2011, Texas Instruments, Incorporated - http://www.ti.com/
 *
 * SPDX-License-Identifier:	GPL-2.0+
 */

#ifndef _BOARD_H_
#define _BOARD_H_

void enable_uart0_pin_mux(void);
void enable_uart2_pin_mux(void);
void enable_uart4_pin_mux(void);
void enable_spi1_mux(void);
void enable_led_mux(void);
void enable_led_mux_v32(void);
void enable_board_pin_mux(void);

#define GPIO_TO_PIN(bank, gpio)		(32 * (bank) + (gpio))

#endif
