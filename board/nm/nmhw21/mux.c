/*
 * mux.c
 *
 * Copyright (C) 2018-2019 NetModule AG - http://www.netmodule.com/
 * Copyright (C) 2011 Texas Instruments Incorporated - http://www.ti.com/
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation version 2.
 *
 * This program is distributed "as is" WITHOUT ANY WARRANTY of any
 * kind, whether express or implied; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 */

#include <common.h>
#include <asm/arch/sys_proto.h>
#include <asm/arch/hardware.h>
#include <asm/arch/mux.h>
#include <asm/io.h>
#include "board.h"

static struct module_pin_mux gpio_pin_mux[] = {
	/*
	* (V2)  GPIO0_8: RS232_485n_SEL (V3.2)
	* (V3)  GPIO0_9: RS485_DE (V3.2)
	* (J18) GPIO0_16: ETH_SW_RST~ (V2.0)
	* (K15) GPIO0_17: CTRL.INT~
	* (T10) GPIO0_23: CAN_TERM1~ (V1.0)
	*
	* (T12) GPIO1_12: SIM_SW
	* (V13) GPIO1_14: GNSS_RST~
	* (U13) GPIO1_15: CAN_TERM0~ (V1.0)
	* (R14) GPIO1_20: BT_EN
	* (V15) GPIO1_21: GSM_PWR_EN
	* (U16) GPIO1_25: RST_GSM
	* (T16) GPIO1_26: WLAN_EN
	* (V17) GPIO1_27: WLAN_IRQ
	*
	* (U3) GPIO2_16: TIMEPULSE (HW26), see note [1]
	* (R6) GPIO2_25: RST_ETH~
	*
	* (J17) GPIO3_4: GNSS_EXTINT
	* (K18) GPIO3_9: CTRL.W_DIS
	* (L18) GPIO3_10: CTRL.RST
	* (C12) GPIO3_17: UI_RST~
	* (A14) GPIO3_21: RST_HUB~ (USB)
	*
	* [1] No PU/PD allowed as TIMEPULSE is internally connected with SAFEBOOT_N.
	*     SAFEBOOT_N must be left open/floating.
	*/

	/* Bank 0 */
	{OFFSET(mii1_txd3), (MODE(7) | PULLUDDIS)},				/* (J18) GPIO0_16: ETH_SW_RST~ (V2.0) */
	{OFFSET(mii1_txd2), (MODE(7) | PULLUDEN | PULLUP_EN | RXACTIVE)},	/* (K15) GPIO0_17: CTRL.INT~ */
	{OFFSET(gpmc_ad9), (MODE(7) | PULLUDDIS)},				/* (T10) GPIO0_23: CAN_TERM1~ */

	/* Bank 1 */
	{OFFSET(gpmc_ad12), (MODE(7) | PULLUDEN | PULLUP_EN)},			/* (T12) GPIO1_12: SIM_SW */
	{OFFSET(gpmc_ad14), (MODE(7) | PULLUDDIS)},				/* (V13) GPIO1_14: GNSS_RST~ */
	{OFFSET(gpmc_ad15), (MODE(7) | PULLUDDIS)},				/* (U13) GPIO1_15: CAN_TERM0~ */

	{OFFSET(gpmc_a4), (MODE(7) | PULLUDDIS)},				/* (R14) gpio1_20: BT_EN */
	{OFFSET(gpmc_a5), (MODE(7) | PULLUDDIS)},				/* (V15) gpio1_21: GSM_PWR_EN */
	{OFFSET(gpmc_a9), (MODE(7) | PULLUDDIS)},				/* (U16) gpio1_25: RST_GSM */
	{OFFSET(gpmc_a10), (MODE(7) | PULLUDDIS)},				/* (T16) gpio1_26: WLAN_EN */
	{OFFSET(gpmc_a11), (MODE(7) | PULLUDDIS | RXACTIVE)},			/* (V17) gpio1_27: WLAN_IRQ */

	/* Bank 2 */
	{OFFSET(lcd_data10), (MODE(7) | PULLUDDIS | RXACTIVE)},			/* (U3) GPIO2_16: TIMEPULSE */
	{OFFSET(lcd_ac_bias_en), (MODE(7) | PULLUDDIS)},			/* (R6) GPIO2_25: RST_ETH~ */

	/* Bank 3 */
	{OFFSET(mii1_rxdv), (MODE(7) | PULLUDDIS)},			/* (J17) GPIO3_4: GNSS_EXTINT */
	{OFFSET(mii1_txclk), (MODE(7) | PULLUDDIS)},			/* (K18) GPIO3_9: CTRL.W_DIS */
	{OFFSET(mii1_rxclk), (MODE(7) | PULLUDDIS)},			/* (L18) GPIO3_10: CTRL.RST~ */
	{OFFSET(mcasp0_ahclkr), (MODE(7) | PULLUDDIS)},			/* (C12) GPIO3_17: UI_RST~ */
	{OFFSET(mcasp0_ahclkx), (MODE(7) | PULLUDDIS)},			/* (A14) GPIO3_21: RST_HUB~ (USB) */
	{-1}
};

static struct module_pin_mux led_pin_mux[] = {
	/*
	* (T17) GPIO0_30: LED0.GN
	* (U15) GPIO1_22: LED1.RD
	* (V16) GPIO1_24: LED1.GN
	* (U18) GPIO1_28: LED0.RD
	*/

	{OFFSET(gpmc_wait0), (MODE(7) | PULLUDDIS)},				/* (T17) GPIO0_30: LED0.GN */
	{OFFSET(gpmc_a6), (MODE(7) | PULLUDDIS)},				/* (U15) GPIO1_22: LED1.RD */
	{OFFSET(gpmc_a8), (MODE(7) | PULLUDDIS)},				/* (V16) GPIO1_24: LED1.GN */
	{OFFSET(gpmc_be1n), (MODE(7) | PULLUDDIS)},				/* (U18) GPIO1_28: LED0.RD */
	{-1}
};

static struct module_pin_mux led_pin_mux_v32[] = {
	/*
	* (C15) GPIO0_6: MB_LED_PWM
	* (U15) GPIO1_22: LED1.RD
	* (T15) GPIO1_23: LED0.GN (formerly: (T17) GPIO0_30)
	* (V16) GPIO1_24: LED1.GN
	* (U18) GPIO1_28: LED0.RD
	*/

	{OFFSET(spi0_cs1), (MODE(7) | PULLUDDIS)},				/* (C15) GPIO0_6: MB_LED_PWM */
	{OFFSET(gpmc_a6), (MODE(7) | PULLUDDIS)},				/* (U15) GPIO1_22: LED1.RD */
	{OFFSET(gpmc_a7), (MODE(7) | PULLUDDIS)},				/* (T15) GPIO1_23: LED0.GN */
	{OFFSET(gpmc_a8), (MODE(7) | PULLUDDIS)},				/* (V16) GPIO1_24: LED1.GN */
	{OFFSET(gpmc_be1n), (MODE(7) | PULLUDDIS)},				/* (U18) GPIO1_28: LED0.RD */
	{-1}
};


/* I2C0 PMIC */
static struct module_pin_mux i2c0_pin_mux[] = {
	{OFFSET(i2c0_sda), (MODE(0) | RXACTIVE | PULLUDEN | PULLUP_EN | SLEWCTRL)},	/* (C17) I2C0_SDA */
	{OFFSET(i2c0_scl), (MODE(0) | RXACTIVE | PULLUDEN | PULLUP_EN | SLEWCTRL)},	/* (C16) I2C0_SCL */
	{-1}
};

/* I2C1 System */
static struct module_pin_mux i2c1_pin_mux[] = {
	{OFFSET(spi0_cs0), (MODE(2) | RXACTIVE | PULLUDEN | PULLUP_EN | SLEWCTRL)},	/* (A16) i2c_scl */
	{OFFSET(spi0_d1), (MODE(2) | RXACTIVE | PULLUDEN | PULLUP_EN | SLEWCTRL)},	/* (B16) i2c_sda */
	{-1}
};

/* RMII1: Ethernet Switch */
static struct module_pin_mux rmii1_pin_mux[] = {
	/* RMII */
	{OFFSET(mii1_crs), MODE(1) | PULLUDDIS | RXACTIVE},			/* (H17) rmii1_crs */
	{OFFSET(mii1_rxerr), MODE(7) | PULLUDEN | PULLDOWN_EN | RXACTIVE},	/* (J15) gpio */
	{OFFSET(mii1_rxd0), MODE(1) | PULLUDDIS | RXACTIVE},			/* (M16) rmii1_rxd0 */
	{OFFSET(mii1_rxd1), MODE(1) | PULLUDDIS | RXACTIVE},			/* (L15) rmii1_rxd1 */
	{OFFSET(mii1_txen), MODE(1) | PULLUDDIS},				/* (J16) rmii1_txen */
	{OFFSET(mii1_txd0), MODE(1) | PULLUDDIS},				/* (K17) rmii1_txd0 */
	{OFFSET(mii1_txd1), MODE(1) | PULLUDDIS},				/* (K16) rmii1_txd1 */
	{OFFSET(rmii1_refclk), MODE(0) | PULLUDDIS | RXACTIVE},			/* (H18) rmii1_refclk */

	/* SMI */
	{OFFSET(mdio_clk), MODE(0) | PULLUDDIS},				/* (M18) mdio_clk */
	{OFFSET(mdio_data), MODE(0) | PULLUDEN | PULLUP_EN | RXACTIVE},		/* (M17) mdio_data */

	/* 25MHz Clock Output */
	{OFFSET(xdma_event_intr0), MODE(3)},					/* (A15) clkout1 (25 MHz clk for Switch) */
	{-1}
};

/* MMC0: WiFi */
static struct module_pin_mux mmc0_sdio_pin_mux[] = {
	{OFFSET(mmc0_clk), (MODE(0) | PULLUDEN | PULLUP_EN | RXACTIVE)},	/* (G17) MMC0_CLK */
	{OFFSET(mmc0_cmd), (MODE(0) | PULLUDEN | PULLUP_EN | RXACTIVE)},	/* (G18) MMC0_CMD */
	{OFFSET(mmc0_dat0), (MODE(0) | PULLUDEN | PULLUP_EN | RXACTIVE)},	/* (G16) MMC0_DAT0 */
	{OFFSET(mmc0_dat1), (MODE(0) | PULLUDEN | PULLUP_EN | RXACTIVE)},	/* (G15) MMC0_DAT1 */
	{OFFSET(mmc0_dat2), (MODE(0) | PULLUDEN | PULLUP_EN | RXACTIVE)},	/* (F18) MMC0_DAT2 */
	{OFFSET(mmc0_dat3), (MODE(0) | PULLUDEN | PULLUP_EN | RXACTIVE)},	/* (F17) MMC0_DAT3 */
	{-1}
};

/* MMC1: eMMC */
static struct module_pin_mux mmc1_emmc_pin_mux[] = {
	{OFFSET(gpmc_csn1), (MODE(2) | PULLUDEN | PULLUP_EN | RXACTIVE)},	/* (U9) MMC1_CLK */
	{OFFSET(gpmc_csn2), (MODE(2) | PULLUDEN | PULLUP_EN | RXACTIVE)},	/* (V9) MMC1_CMD */
	{OFFSET(gpmc_ad0), (MODE(1) | PULLUDEN | PULLUP_EN | RXACTIVE)},	/* (U7) MMC1_DAT0 */
	{OFFSET(gpmc_ad1), (MODE(1) | PULLUDEN | PULLUP_EN | RXACTIVE)},	/* (V7) MMC1_DAT1 */
	{OFFSET(gpmc_ad2), (MODE(1) | PULLUDEN | PULLUP_EN | RXACTIVE)},	/* (R8) MMC1_DAT2 */
	{OFFSET(gpmc_ad3), (MODE(1) | PULLUDEN | PULLUP_EN | RXACTIVE)},	/* (T8) MMC1_DAT3 */
	{OFFSET(gpmc_ad4), (MODE(1) | PULLUDEN | PULLUP_EN | RXACTIVE)},	/* (U8) MMC1_DAT4 */
	{OFFSET(gpmc_ad5), (MODE(1) | PULLUDEN | PULLUP_EN | RXACTIVE)},	/* (V8) MMC1_DAT5 */
	{OFFSET(gpmc_ad6), (MODE(1) | PULLUDEN | PULLUP_EN | RXACTIVE)},	/* (R9) MMC1_DAT6 */
	{OFFSET(gpmc_ad7), (MODE(1) | PULLUDEN | PULLUP_EN | RXACTIVE)},	/* (T9) MMC1_DAT7 */
	{-1}
};

/* USB_DRVBUS not used -> configure as GPIO */
static struct module_pin_mux usb_pin_mux[] = {
	{OFFSET(usb0_drvvbus), (MODE(7) | PULLUDDIS)},				/* (F16) USB0_DRVVBUS */
	{OFFSET(usb1_drvvbus), (MODE(7) | PULLUDDIS)},				/* (F15) USB1_DRVVBUS */
	{-1}
};


/* TODO: SPI in operational mode */
/* What in XModem SPL Boot */

/* SPI1 */
static struct module_pin_mux spi1_pin_mux[] = {
	{OFFSET(ecap0_in_pwm0_out), (MODE(4) | PULLUDEN | PULLUP_EN)},		/* (C18) spi1_clk */
	{OFFSET(uart0_rtsn), (MODE(4) | PULLUDEN | PULLUP_EN)},			/* (E17) spi1_mosi */
	{OFFSET(uart0_ctsn), (MODE(4) | PULLUDEN | PULLUP_EN | RXACTIVE)},	/* (E18) spi1_miso */
	{OFFSET(uart0_rxd), (MODE(1) | PULLUDEN | PULLUP_EN)},			/* (E15) spi1_cs0 */
	{OFFSET(uart0_txd), (MODE(1) | PULLUDEN | PULLUP_EN)},			/* (E16) spi1_cs1 */
	{-1}
};

/* CAN0: */
static struct module_pin_mux can0_pin_mux[] = {
	{OFFSET(uart1_rtsn), (MODE(2) | RXACTIVE | PULLUDEN | PULLUP_EN)},	/* (D17) can0_rxd */
	{OFFSET(uart1_ctsn), (MODE(2) | PULLUDEN | PULLUP_EN)},			/* (D18) can0_txd */
	{-1}
};

/* CAN1: */
static struct module_pin_mux can1_pin_mux[] = {
	{OFFSET(uart1_txd), (MODE(2) | RXACTIVE | PULLUDEN | PULLUP_EN)},	/* (D15) can1_rxd */
	{OFFSET(uart1_rxd), (MODE(2) | PULLUDEN | PULLUP_EN)},			/* (D16) can1_txd */
	{-1}
};

/* UART2: Debug/Console */
static struct module_pin_mux uart2_pin_mux[] = {
	{OFFSET(spi0_sclk), (MODE(1) | PULLUDEN | PULLUP_EN | RXACTIVE)},	/* (A17) uart2_rxd */
	{OFFSET(spi0_d0), (MODE(1) | PULLUDEN | PULLUP_EN)},			/* (B17) uart2_txd */
	{-1}
};

/* UART3: GNSS */
static struct module_pin_mux uart3_pin_mux[] = {
	{OFFSET(mii1_rxd3), (MODE(1) | PULLUDDIS | RXACTIVE)},			/* (L17) UART3_RXD */
	{OFFSET(mii1_rxd2), (MODE(1) | PULLUDDIS | SLEWCTRL)},			/* (L16) UART3_TXD */
	{-1}
};

/* UART4: User RS232/485 (V3.2 only) */
static struct module_pin_mux uart4_pin_mux[] = {
	/*
	 * CTSn = SEL_RS232/RS485~: Default = Low -> RS485 mode
	 * RTSn = RS485_DE: Default = Low -> RS485 transmitter disabled
	 *        Configure as GPIO in U-Boot to keep disabled, Linux will change to RTSn
	 */
	{OFFSET(gpmc_wait0), (MODE(6) | PULLUDEN | PULLUP_EN | RXACTIVE)},	/* (T17) UART4_RXD */
	{OFFSET(gpmc_wpn), (MODE(6) | PULLUDEN | PULLUP_EN | SLEWCTRL)},	/* (U17) UART4_TXD */
	{OFFSET(lcd_data12), (MODE(7) | PULLUDEN | PULLDOWN_EN)},		/* (V2) uart4_ctsn */
	{OFFSET(lcd_data13), (MODE(7) | PULLUDEN | PULLDOWN_EN)},		/* (V3) uart4_rtsn */
	{-1}
};

/* UART5: Highspeed UART for Bluetooth (no SLEWCTRL) */
static struct module_pin_mux uart5_pin_mux[] = {
	{OFFSET(lcd_data9), (MODE(4) | PULLUDEN | PULLUP_EN | RXACTIVE)},	/* (U2) UART5_RXD */
	{OFFSET(lcd_data8), (MODE(4) | PULLUDEN | PULLUP_EN)},			/* (U1) UART5_TXD */
	{OFFSET(lcd_data14), (MODE(6) | PULLUDEN | PULLUP_EN | RXACTIVE)},	/* (V4) uart5_ctsn */
	{OFFSET(lcd_data15), (MODE(6) | PULLUDEN | PULLUP_EN)},			/* (T5) uart5_rtsn */
	{-1}
};

static struct module_pin_mux unused_pin_mux[] = {
	/* SYSBOOT6, 7, 10, 11: Not used pulldown active, receiver disabled */
	{OFFSET(lcd_data6), (MODE(7) | PULLUDEN | PULLDOWN_EN)},
	{OFFSET(lcd_data7), (MODE(7) | PULLUDEN | PULLDOWN_EN)},
	{OFFSET(lcd_data11), (MODE(7) | PULLUDEN | PULLDOWN_EN)},

	/* TODO: GPMCA1..3, A6..8 */

	{-1}
};

/* UART0: <tbd> */
static struct module_pin_mux uart0_pin_mux[] = {
	{OFFSET(uart0_rxd), (MODE(0) | PULLUDEN | PULLUP_EN | RXACTIVE)},	/* (E15) UART0_RXD */
	{OFFSET(uart0_txd), (MODE(0) | PULLUDEN | PULLUP_EN | SLEWCTRL)},	/* (E16) UART0_TXD */
	{-1}
};

void enable_board_pin_mux(void)
{
	configure_module_pin_mux(gpio_pin_mux);

	configure_module_pin_mux(rmii1_pin_mux);
	configure_module_pin_mux(mmc0_sdio_pin_mux);
	configure_module_pin_mux(mmc1_emmc_pin_mux);
	configure_module_pin_mux(usb_pin_mux);

	configure_module_pin_mux(i2c0_pin_mux);
	configure_module_pin_mux(i2c1_pin_mux);
/*	configure_module_pin_mux(spi1_pin_mux); */
	/* TODO: SPL Bringup */

	configure_module_pin_mux(uart3_pin_mux);
	configure_module_pin_mux(uart5_pin_mux);

	configure_module_pin_mux(can0_pin_mux);
	configure_module_pin_mux(can1_pin_mux);

	configure_module_pin_mux(unused_pin_mux);
}

void enable_uart0_pin_mux(void)
{
	configure_module_pin_mux(uart0_pin_mux);
}

void enable_uart2_pin_mux(void)
{
	configure_module_pin_mux(uart2_pin_mux);
}

void enable_uart4_pin_mux(void)
{
	configure_module_pin_mux(uart4_pin_mux);
}

void enable_spi1_mux(void)
{
	configure_module_pin_mux(spi1_pin_mux);
}

void enable_led_mux(void)
{
	configure_module_pin_mux(led_pin_mux);
}

void enable_led_mux_v32(void)
{
	configure_module_pin_mux(led_pin_mux_v32);
}
