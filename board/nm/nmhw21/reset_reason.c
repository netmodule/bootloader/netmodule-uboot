/*
 * reset_reason.c
 *
 * Reset/start reason handling
 *
 * Copyright (C) 2021 NetModule AG - https://www.netmodule.com/
 *
 * SPDX-License-Identifier:	GPL-2.0+
 */
#include <common.h>
#include "../common/ether_crc.h"
#include "reset_reason.h"


void rr_set_reset_reason(volatile struct reset_registers* reset_regs, uint32_t reason)
{
	reset_regs->rr_value = reason;
	reset_regs->rr_value_crc = ether_crc(sizeof(reset_regs->rr_value),
					     (const uint8_t*)&(reset_regs->rr_value));
}

bool rr_is_reset_reason_valid(volatile const struct reset_registers* reset_regs)
{
	const uint32_t crc = ether_crc(sizeof(reset_regs->rr_value), 
				       (const uint8_t*)&(reset_regs->rr_value));
	return crc == reset_regs->rr_value_crc;
}

void rr_set_start_reason(volatile struct reset_registers* reset_regs, uint32_t event)
{
	/* Store start events in shared memory region for OS */
	reset_regs->sr_magic = SR_MAGIC;
	reset_regs->sr_events = event;
	reset_regs->sr_checksum = ether_crc(sizeof(reset_regs->sr_events),
					    (const uint8_t*)&(reset_regs->sr_events));	
}

bool rr_is_start_reason_valid(volatile const struct reset_registers* reset_regs)
{
	if (reset_regs->sr_magic == SR_MAGIC) {
		const uint32_t crc = ether_crc(sizeof(reset_regs->sr_events), 
					(const uint8_t*)&(reset_regs->sr_events));
		if (crc == reset_regs->sr_checksum) {
			return true;
		}
	}
	
	return false;
}

void rr_start_reason_to_str(uint32_t events, char* buffer, size_t bufsize)
{
	if (events == 0) {
		strncpy(buffer, "-\n", bufsize);
	}
	else {
		buffer[0] = 0;
		if (events & SR_POR)
			strncat(buffer, "PowerOn, ", bufsize);
		if (events & SR_WATCHDOG)
			strncat(buffer, "Watchdog, ", bufsize);
		if (events & SR_REBOOT)
			strncat(buffer, "Reboot, ", bufsize);
		if (events & SR_WAKEUP)
			strncat(buffer, "Wakeup, ", bufsize);

		if (events & SR_EVT_IGNITION)
			strncat(buffer, "Ignition, ", bufsize);
		if (events & SR_EVT_RTC_ALARM)
			strncat(buffer, "RTC, ", bufsize);
		if (events & SR_EVT_RTC_TICK)
			strncat(buffer, "Tick, ", bufsize);
		if (events & SR_EVT_GPI)
			strncat(buffer, "GPI, ", bufsize);
		if (events & SR_EVT_BUTTON)
			strncat(buffer, "Button, ", bufsize);

		/* Trim last comma, no 0 len check required, at least one entry is present */
		buffer[strlen(buffer)-2] = 0;
	}
}
