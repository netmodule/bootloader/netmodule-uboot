/*
 * reset_reason.h
 *
 * Reset/start reason handling
 *
 * Copyright (C) 2021 NetModule AG - https://www.netmodule.com/
 *
 * SPDX-License-Identifier:	GPL-2.0+
 */

#ifndef RESET_REASON_H
#define RESET_REASON_H

struct reset_registers {
	/* Reboot Reasons, set by OS, expect watchdog set by bootloader */
	uint32_t rr_value;
	uint32_t rr_value_crc;

	/* Start Reasons as determined by hardware */
	uint32_t sr_magic;	/* Token to check presence of following fields */
	uint32_t sr_events;	/* Events bitmask, see SE_... defines */
	uint32_t sr_checksum;	/* Checksum over se_events */
};

/* Watchdog reboot reason event */
#define RR_POWEROFF_PATTERN		0x00000000
#define RR_EXTERNAL_WATCHDOG_PATTERN	0x781f9ce2
#define RR_BOOT_PATTERN			0x424f4f54	/* ‘BOOT’, 0xb9808470 */
#define RR_REBOOT_PATTERN		0x5245424f	/* ‘REBO’, 0x7d5d9d66 */
#define RR_OOPS_PATTERN			0x4F4F5053	/* ‘OOPS’, 0x2b85bc5f */
#define RR_WAKE_PATTERN			0x57414B45	/* 'WAKE', 0x7b0acb48 */

/* Start reason token 'SRTE' */
#define SR_MAGIC			0x53525445

/* Possible start events (see sr_events) */
#define SR_POR  			0x00000001
#define SR_WATCHDOG  			0x00000010
#define SR_REBOOT  			0x00000020
#define SR_WAKEUP  			0x00000080	/* See SR_EVT_xx bits */

/* In case of wake-up, these are the events that caused the start */
#define SR_EVT_IGNITION  		0x00000100
#define SR_EVT_RTC_ALARM  		0x00000200	/* RTC date/time alarm */
#define SR_EVT_RTC_TICK  		0x00000400	/* RTC tick based alarm */
#define SR_EVT_GPI			0x00000800	/* General purpose input(s) */
#define SR_EVT_BUTTON			0x00001000	
#define SR_EVT_WAKE_MASK		0x00001F00



extern void rr_set_reset_reason(volatile struct reset_registers* reset_regs, uint32_t reason);
extern bool rr_is_reset_reason_valid(volatile const struct reset_registers* reset_regs);

extern void rr_set_start_reason(volatile struct reset_registers* reset_regs, uint32_t event);
extern bool rr_is_start_reason_valid(volatile const struct reset_registers* reset_regs);
extern void rr_start_reason_to_str(uint32_t events, char* buffer, size_t bufsize);


#endif /* RESET_REASON_H */
