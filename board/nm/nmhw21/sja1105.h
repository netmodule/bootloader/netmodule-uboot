/*
 * sja1105.c
 *
 * Functions for NXP SJA1105 Ethernet Switch
 *
 * Copyright (C) 2018 NetModule AG - http://www.netmodule.com/
 *
 * SPDX-License-Identifier:	GPL-2.0+
 */
#ifndef _SJA1105_H_
#define _SJA1105_H_

#define SJA_REG_DEVICE_ID	0x000000
#define SJA_REG_CONFIG_STATUS	0x000001

/**
 * Initializes the sja1105 driver.
 *
 * Must be called before any other function can be used.
 * Takes and remembers SPI driver for later calls.
 *
 * @param spi	SPI driver instance
 */
void sja1105_init(struct spi_slave *spi);

/**
 * Claims bus for subsequent switch accesses.
 */
void sja1105_claim_bus(void);

/**
 * Releases bus.
 */
void sja1105_release_bus(void);

/**
 * Reads switch register.
 *
 * (multiple register not possible with this function)
 *
 * @param address	register to read (range 0x0 to 0x100BC3)
 * @returns readback data
 */
uint32_t sja1105_read_reg(uint32_t address);

/**
 * Writes switch register.
 *
 * @param address	register to write (range 0x0 to 0x100BC3)
 * @param data		data to write
 */
void sja1105_write_reg(uint32_t address, uint32_t data);

/**
 * Loads switch firmware.
 *
 * This function must be called after the switch has been powered up
 *
 * When the device is powered up, it expects to receive an input stream
 * containing initial setup information over the configuration interface.
 * The initial configuration data sets the port modes, sets up VLANs
 * and defines other forwarding and quality-of-service rules.
 *
 * This function takes care of loading the configuration according to the
 * SJA1105 user manual.
 */
void sja1105_configure_firmware(void);

/**
 * Configures the PLL, the CGU and the Auxiliary Configuration Unit.
 *
 * Notes: sja1105_configure_firmware() must be called prior to this function.
 *
 * Mode and clock description :
 * - RMII operation on all ports:
 * - PLL 1 setup for 50Mhz
 * - Port 0 and 4: RMII (PHY mode = external REFCLK)
 * - Port 1,2 and 3: RMII (MAC mode)
 */
void sja1105_configure_mode_and_clocks(void);

/**
 * Configures IO pads to a safe state.
 *
 * Unused I/Os: Sets pull down to unused pins and set drive strengths
 */
void sja1105_configure_io(void);

#endif /* _SJA1105_H_ */
