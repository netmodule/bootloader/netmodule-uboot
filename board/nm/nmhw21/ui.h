/*
 * ui.c
 *
 * User Interface access
 *
 * Copyright (C) 2018 NetModule AG - http://www.netmodule.com/
 *
 * SPDX-License-Identifier:	GPL-2.0+
 */

#ifndef UI_H
#define UI_H


#define CONFIG_UI_I2C_BUS	1

#define CONFIG_UI_V1_I2C_ADDR	0x74
#define CONFIG_UI_V2_I2C_ADDR	0x70


/**
 * Initializes user interface module.
 *
 * @param i2c_bus Number of I2C bus UI is attached to.
 */
extern void ui_init(int i2c_bus);

/**
 * Returns hardware version of UI.
 *
 * @return 0: No or unknown UI
 *         >0: Version (e.g. 1 or 2)
 */
extern int ui_version(void);

void ui_set_top_led(int red, int green);
void ui_set_bottom_led(int red, int green);

#endif /* UI_H */
