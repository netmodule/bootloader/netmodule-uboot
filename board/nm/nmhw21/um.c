/*
 * um.c
 *
 * User Module Access
 *
 * Copyright (C) 2019 NetModule AG - http://www.netmodule.com/
 *
 * SPDX-License-Identifier:	GPL-2.0+
 */

#include <common.h>
#include <errno.h>
#include <i2c.h>

#include "um.h"


static int um_i2c_bus = 0;
static int bus_claimed = 0;

static int module_present = 0;
static um_type_t module_type = UM_TYPE_RESERVED;
static int hw_version = 0;
static int hw_revision = 0;
static struct in_addr ipv4_addr;
static struct in_addr ipv4_mask;


static int switch_i2c_bus(int* old_bus)
{
	int ret = 0;

        if (old_bus == NULL)
                return -1;

	*old_bus = i2c_get_bus_num();
	if (*old_bus != um_i2c_bus) {
		ret = i2c_set_bus_num(um_i2c_bus);
	}

	bus_claimed++;

	return ret;
}

static void revert_i2c_bus(int bus)
{
	if (um_i2c_bus != bus) {
		(void)i2c_set_bus_num(bus);
	}

	bus_claimed--;
}

static void get_version(void)
{
	int ret;
	uint8_t reg[1];

	ret = i2c_read(CONFIG_UM_I2C_ADDR, UM_REG_HW_VER, 1, reg, 1);
	if (ret == 0) {
		hw_version  = (reg[0] >> 4) & 0x0F;
		hw_revision = (reg[0] >> 0) & 0x0F;
	} else {
		puts("error reading user module version\n");
	}
}


static void set_inaddr(const uint8_t* data, struct in_addr* in)
{
	in->s_addr = htonl(data[0] << 24 | data[1] << 16 | data[2] << 8 | data[3] << 0);	
}

static void get_network_address(void)
{
	int ret;
	uint8_t data[8];

	ret = i2c_read(CONFIG_UM_I2C_ADDR, UM_REG_IPV4_ADDR, 1, data, 8);
	if (ret == 0) {
		set_inaddr(&data[0], &ipv4_addr);
		set_inaddr(&data[4], &ipv4_mask);
	} else {
		puts("error reading user module network configuration\n");
	}
}

static void detect(void)
{
	int ret;
	int i;
	uint8_t reg[2];

	hw_version = 0;

	/* We don't know how fast the UM boots, try for up to 500 msecs */
	for (i=0; i<500/10; i++) {
		/* Try to read detect and type register */ 
		ret = i2c_read(CONFIG_UM_I2C_ADDR, UM_REG_PRESENCE, 1, reg, 2);
		if (ret == 0) {
			/* i2c read was successful, now check presence register */
			if (reg[UM_REG_PRESENCE] == UM_PRESENCE_TOKEN) {
				module_present = 1;

				module_type = (um_type_t)(reg[UM_REG_TYPE]);
			}
			break;
		}

		udelay(10*1000);	/* 10 ms */
	}
}


void um_init(int i2c_bus)
{
	int bus = -1;
	int claimed;

	um_i2c_bus = i2c_bus;

	claimed = switch_i2c_bus(&bus);
	
	if (claimed == 0) {
		detect();

		if (module_present) {
			/* TODO: Check why this delay is required.
			 * Is the UM software still reading board descriptor? 
			 */
			udelay(50*1000);	/* 50 ms delay (tested with 30 ms) */

			get_version();
			get_network_address();
		}
	}

	revert_i2c_bus(bus);
}

int um_present(void) 
{
	return module_present;
}

const char* um_type_as_str(void) 
{
	switch (module_type) {
		case UM_TYPE_VCUPRO: return "VCU Pro";
		default: return "<unknown";
	}
}

void um_version(uint8_t *version, uint8_t *revision)
{
	if (version != NULL)
		*version = hw_version;

	if (revision != NULL)
		*revision = hw_revision;
}

void um_network_address(struct in_addr* ip, struct in_addr* mask)
{
	if (ip != NULL)
		*ip = ipv4_addr;

	if (mask != NULL)
		*mask = ipv4_mask;
}
