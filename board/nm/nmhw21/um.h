/*
 * um.h
 *
 * User Module Access
 *
 * Copyright (C) 2019 NetModule AG - http://www.netmodule.com/
 *
 * SPDX-License-Identifier:	GPL-2.0+
 */

#ifndef UM_H
#define UM_H

#include <net.h>


#define CONFIG_UM_I2C_BUS	1
#define CONFIG_UM_I2C_ADDR	0x40

#define UM_REG_PRESENCE		0x00	/* Presence register, value = 0xC5 */
#define UM_REG_TYPE		0x01	/* See um_type_t */
#define UM_REG_HW_VER		0x04	/* Version.Revision, e.g. 0x21 = v2.1 */

#define UM_REG_IPV4_ADDR	0x10	/* IPv4 address in network byte order, 4 bytes */
#define UM_REG_IPV4_MASK	0x14	/* IPv4 mask in network byte order, 4 bytes */

#define UM_PRESENCE_TOKEN	0xC5


/* Known user modules */
typedef enum {
	UM_TYPE_RESERVED = 0,
	UM_TYPE_VCUPRO = 1,

	UM_TYPE_MAX
} um_type_t;


/**
 * Initializes user user module.
 *
 * @param i2c_bus Number of I2C bus module is attached to.
 */
void um_init(int i2c_bus);

/**
 * Returns whether a module has been detected. 
 *  
 * @return 1 if module is detected, 0 otherwise.
 */
int um_present(void);

/**
 * Returns name of module.
 * 
 * @return Name of module or "<unknown>".
 */
const char* um_type_as_str(void);

/**
 * Returns hardware version of module.
 * 
 * @param version Pointer to variable to receive module version (0..15)
 * @param revision Pointer to variable to receive module revision (0..15)
 */
void um_version(uint8_t *version, uint8_t *revision);

/**
 * Returns user module IPv4 configuration 
 * 
 * @param ip Pointer to variable to receive IPv4 address
 * @param mask Pointer to variable to receive IPv4 mask
 */
void um_network_address(struct in_addr* ip, struct in_addr* mask);

#endif /* UM_H */
