#ifndef SHIELD_CAN_H
#define SHIELD_CAN_H

int shield_can_init(void);
int shield_can_setmode(int mode);

void can_shield_init(void);
void can_shield_passive_init(void);

#endif // SHIELD_CAN_H
