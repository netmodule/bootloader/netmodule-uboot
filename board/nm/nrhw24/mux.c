/*
 * mux.c
 *
 * Copyright (C) 2018-2019 NetModule AG - http://www.netmodule.com/
 * Copyright (C) 2011 Texas Instruments Incorporated - http://www.ti.com/
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation version 2.
 *
 * This program is distributed "as is" WITHOUT ANY WARRANTY of any
 * kind, whether express or implied; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 */

#include <common.h>
#include <asm/arch/sys_proto.h>
#include <asm/arch/hardware.h>
#include <asm/arch/mux.h>
#include <asm/io.h>
#include "board.h"

static struct module_pin_mux gpio_pin_mux[] = {
	/*
	* CPU GPIOs
	*
	* (A17) GPIO0_2: RST_GNSS~
	* (A16) GPIO0_5: EXTINT_GNSS
	* (C15) GPIO0_6: TIMEPULSE
	* (C18) GPIO0_7: PWM / SHIELD LATCH
	* (J18) GPIO0_16: RST_PHY~
	* (U12) GPIO0_27: RST_SHIELD~
	*
	* (R14) GPIO1_20: BT_EN
	* (V15) GPIO1_21: GSM_PWR_EN
	* (U16) GPIO1_25: RST_GSM
	* (T16) GPIO1_26: WLAN_EN
	* (V17) GPIO1_27: WLAN_IRQ
	*
	* (C12) GPIO3_17: SIM_SEL
	*/

	/* Bank 0 */
	{OFFSET(spi0_sclk), (MODE(7) | PULLUDDIS)},				/* (A17) gpio0[2] */  /* RST_GNSS */
	{OFFSET(spi0_cs0), (MODE(7) | PULLUDEN | PULLDOWN_EN)},			/* (A16) gpio0[5] */  /* EXTINT_GNSS */
	{OFFSET(spi0_cs1), (MODE(7) | PULLUDEN | PULLUP_EN | RXACTIVE)},	/* (C15) gpio0[6] */  /* TIMEPULSE */
	{OFFSET(ecap0_in_pwm0_out), (MODE(7) | PULLUDEN | PULLUP_EN)},		/* (C18) gpio0[7] */  /* IO_SHIELD */
	{OFFSET(mii1_txd3), (MODE(7) | PULLUDDIS)},				/* (J18) gpio0[16] */  /* RST_PHY~ */
	{OFFSET(gpmc_ad11), (MODE(7) | PULLUDDIS)},				/* (U12) gpio0[27] */  /* RST_SHIELD~ */

	/* Bank 1 */
	{OFFSET(gpmc_a4), (MODE(7) | PULLUDDIS)},				/* (R14) gpio1[20] */  /* BT_EN */
	{OFFSET(gpmc_a5), (MODE(7) | PULLUDDIS)},				/* (V15) gpio1[21] */  /* GSM_PWR_EN */
	{OFFSET(gpmc_a9),  (MODE(7) | PULLUDDIS)},				/* (U16) gpio1[25] */  /* RST_GSM */
	{OFFSET(gpmc_a10), (MODE(7) | PULLUDDIS)},				/* (T16) gpio1[26] */  /* WLAN_EN */
	{OFFSET(gpmc_a11),  (MODE(7) | PULLUDDIS | RXACTIVE)},			/* (V17) gpio1[27] */  /* WLAN_IRQ */

	/* TODO: What about all the unused GPMC pins ? */

	/* Bank 2 */
#if 0
	/* TODO: What is this meant for? */
	{OFFSET(lcd_data3), (MODE(7) | PULLUDEN | PULLUP_EN)},	/* (R4) gpio2[9] */  /* SYSBOOT_3 */
	{OFFSET(lcd_data4), (MODE(7) | PULLUDEN | PULLUP_EN)},	/* (T1) gpio2[10] */  /* SYSBOOT_4 */

	/* TODO: Check other unued pins from sysboot block */
	/* Ensure PU/PD does not work against external signal */
	/*
	 * SYSBOOT 0,1,5,12,13 = Low
	 * SYSBOOT 2 = High
	 */
#endif

	/* Bank 3 */
	{OFFSET(mcasp0_ahclkr), (MODE(7) | PULLUDEN | PULLDOWN_EN)},		/* (C12) gpio3[17] */  /* SIM_SEL */
	{-1}
};

/* I2C0 PMIC */
static struct module_pin_mux i2c0_pin_mux[] = {
	{OFFSET(i2c0_sda), (MODE(0) | RXACTIVE | PULLUDEN | PULLUP_EN | SLEWCTRL)},	/* (C17) I2C0_SDA */
	{OFFSET(i2c0_scl), (MODE(0) | RXACTIVE | PULLUDEN | PULLUP_EN | SLEWCTRL)},	/* (C16) I2C0_SCL */
	{-1}
};

/* I2C2 System */
static struct module_pin_mux i2c2_pin_mux[] = {
	{OFFSET(uart1_rtsn), (MODE(3) | RXACTIVE | PULLUDEN | PULLUP_EN | SLEWCTRL)},	/* (D17) I2C2_SCL */
	{OFFSET(uart1_ctsn), (MODE(3) | RXACTIVE | PULLUDEN | PULLUP_EN | SLEWCTRL)},	/* (D18) I2C2_SDA */
	{-1},
};

/* RMII1: Ethernet */
static struct module_pin_mux rmii1_pin_mux[] = {
	/* RMII */
	{OFFSET(mii1_crs), MODE(1) | PULLUDDIS | RXACTIVE},			/* (H17) rmii1_crs */
	{OFFSET(mii1_rxerr), MODE(7) | PULLUDEN | PULLDOWN_EN | RXACTIVE},	/* (J15) gpio */
	{OFFSET(mii1_rxd0), MODE(1) | PULLUDDIS | RXACTIVE},			/* (M16) rmii1_rxd0 */
	{OFFSET(mii1_rxd1), MODE(1) | PULLUDDIS | RXACTIVE},			/* (L15) rmii1_rxd1 */
	{OFFSET(mii1_txen), MODE(1) | PULLUDDIS},				/* (J16) rmii1_txen */
	{OFFSET(mii1_txd0), MODE(1) | PULLUDDIS},				/* (K17) rmii1_txd0 */
	{OFFSET(mii1_txd1), MODE(1) | PULLUDDIS},				/* (K16) rmii1_txd1 */
	{OFFSET(rmii1_refclk), MODE(0) | PULLUDDIS | RXACTIVE},			/* (H18) rmii1_refclk */

	/* SMI */
	{OFFSET(mdio_clk), MODE(0) | PULLUDDIS},				/* (M18) mdio_clk */
	{OFFSET(mdio_data), MODE(0) | PULLUDEN | PULLUP_EN | RXACTIVE},		/* (M17) mdio_data */

	/* 25MHz Clock Output */
	{OFFSET(xdma_event_intr0), MODE(3)},					/* (A15) clkout1 (25 MHz clk for PHY) */
	{-1}
};

/* MMC0: WiFi */
static struct module_pin_mux mmc0_sdio_pin_mux[] = {
	{OFFSET(mmc0_clk), (MODE(0) | PULLUDEN | PULLUP_EN | RXACTIVE)},	/* (G17) MMC0_CLK */
	{OFFSET(mmc0_cmd), (MODE(0) | PULLUDEN | PULLUP_EN | RXACTIVE)},	/* (G18) MMC0_CMD */
	{OFFSET(mmc0_dat0), (MODE(0) | PULLUDEN | PULLUP_EN | RXACTIVE)},	/* (G16) MMC0_DAT0 */
	{OFFSET(mmc0_dat1), (MODE(0) | PULLUDEN | PULLUP_EN | RXACTIVE)},	/* (G15) MMC0_DAT1 */
	{OFFSET(mmc0_dat2), (MODE(0) | PULLUDEN | PULLUP_EN | RXACTIVE)},	/* (F18) MMC0_DAT2 */
	{OFFSET(mmc0_dat3), (MODE(0) | PULLUDEN | PULLUP_EN | RXACTIVE)},	/* (F17) MMC0_DAT3 */
	{-1}
};

/* MMC1: eMMC */
static struct module_pin_mux mmc1_emmc_pin_mux[] = {
	{OFFSET(gpmc_csn1), (MODE(2) | PULLUDEN | PULLUP_EN | RXACTIVE)},	/* (U9) MMC1_CLK */
	{OFFSET(gpmc_csn2), (MODE(2) | PULLUDEN | PULLUP_EN | RXACTIVE)},	/* (V9) MMC1_CMD */
	{OFFSET(gpmc_ad0), (MODE(1) | PULLUDEN | PULLUP_EN | RXACTIVE)},	/* (U7) MMC1_DAT0 */
	{OFFSET(gpmc_ad1), (MODE(1) | PULLUDEN | PULLUP_EN | RXACTIVE)},	/* (V7) MMC1_DAT1 */
	{OFFSET(gpmc_ad2), (MODE(1) | PULLUDEN | PULLUP_EN | RXACTIVE)},	/* (R8) MMC1_DAT2 */
	{OFFSET(gpmc_ad3), (MODE(1) | PULLUDEN | PULLUP_EN | RXACTIVE)},	/* (T8) MMC1_DAT3 */
	{OFFSET(gpmc_ad4), (MODE(1) | PULLUDEN | PULLUP_EN | RXACTIVE)},	/* (U8) MMC1_DAT4 */
	{OFFSET(gpmc_ad5), (MODE(1) | PULLUDEN | PULLUP_EN | RXACTIVE)},	/* (V8) MMC1_DAT5 */
	{OFFSET(gpmc_ad6), (MODE(1) | PULLUDEN | PULLUP_EN | RXACTIVE)},	/* (R9) MMC1_DAT6 */
	{OFFSET(gpmc_ad7), (MODE(1) | PULLUDEN | PULLUP_EN | RXACTIVE)},	/* (T9) MMC1_DAT7 */
	{-1}
};

/* USB_DRVBUS not used -> configure as GPIO */
static struct module_pin_mux usb_pin_mux[] = {
	{OFFSET(usb0_drvvbus), (MODE(7) | PULLUDDIS)},				/* (F16) USB0_DRVVBUS */
	{OFFSET(usb1_drvvbus), (MODE(7) | PULLUDDIS)},				/* (F15) USB1_DRVVBUS */
	{-1}
};

/* UART0: RS232/RS485 shield mode */
static struct module_pin_mux uart0_pin_mux[] = {
	{OFFSET(uart0_rxd), (MODE(0) | PULLUDEN | PULLUP_EN | RXACTIVE)},	/* (E15) UART0_RXD */
	{OFFSET(uart0_txd), (MODE(0) | PULLUDEN | PULLUP_EN | SLEWCTRL)},	/* (E16) UART0_TXD */
	{-1},
};

/* UART0: Shield I/F (UART, CAN) */
/* Leave UART0 unconfigured because we want to configure it as needed by Linux (can/spi/uart/etc) */
/* Mode 7 = GPIO */
static struct module_pin_mux uart0_disabled_pin_mux[] = {
	{OFFSET(uart0_rxd), (MODE(7) | PULLUDDIS | RXACTIVE)},			/* (E15) GPIO1_10 */
	{OFFSET(uart0_txd), (MODE(7) | PULLUDDIS | RXACTIVE)},			/* (E16) GPIO1_11 */
	{OFFSET(uart0_ctsn), (MODE(7) | PULLUDDIS | RXACTIVE)},			/* (E18) GPIO1_8 */
	{OFFSET(uart0_rtsn), (MODE(7) | PULLUDEN | PULLUP_EN)},			/* (E17) GPIO1_9 */
	{-1},
};

/* UART1: User (Debug/Console) */
static struct module_pin_mux uart1_pin_mux[] = {
	{OFFSET(uart1_rxd), (MODE(0) | PULLUDEN | PULLUP_EN | RXACTIVE)},	/* (D16) uart1_rxd */
	{OFFSET(uart1_txd), (MODE(0) | PULLUDEN | PULLUP_EN | SLEWCTRL)},	/* (D15) uart1_txd */
	{-1},
};

/* UART3: GNSS */
static struct module_pin_mux uart3_pin_mux[] = {
	{OFFSET(mii1_rxd3), (MODE(1) | PULLUDEN | PULLUP_EN | RXACTIVE)},	/* (L17) UART3_RXD */
	{OFFSET(mii1_rxd2), (MODE(1) | PULLUDEN | PULLUP_EN | SLEWCTRL)},	/* (L16) UART3_TXD */
	{-1}
};

/* UART5: Highspeed UART for Bluetooth (no SLEWCTRL) */
static struct module_pin_mux uart5_pin_mux[] = {
	{OFFSET(lcd_data9), (MODE(4) | PULLUDEN | PULLUP_EN | RXACTIVE)},	/* (U2) UART5_RXD */
	{OFFSET(lcd_data8), (MODE(4) | PULLUDEN | PULLUP_EN)},			/* (U1) UART5_TXD */
	{OFFSET(lcd_data14), (MODE(6) | PULLUDEN | PULLUP_EN | RXACTIVE)},	/* (V4) uart5_ctsn */
	{OFFSET(lcd_data15), (MODE(6) | PULLUDEN | PULLUP_EN)},			/* (T5) uart5_rtsn */
	{-1}
};

static struct module_pin_mux unused_pin_mux[] = {
	/* SYSBOOT6, 7, 10, 11: Not used pulldown active, receiver disabled */
	{OFFSET(lcd_data6), (MODE(7) | PULLUDEN | PULLDOWN_EN)},
	{OFFSET(lcd_data7), (MODE(7) | PULLUDEN | PULLDOWN_EN)},
	{OFFSET(lcd_data10), (MODE(7) | PULLUDEN | PULLDOWN_EN)},
	{OFFSET(lcd_data11), (MODE(7) | PULLUDEN | PULLDOWN_EN)},

	/* TODO: GPMCA1..3, A6..8 */

	{-1}
};


void enable_board_pin_mux(void)
{
	configure_module_pin_mux(gpio_pin_mux);

	configure_module_pin_mux(rmii1_pin_mux);
	configure_module_pin_mux(mmc0_sdio_pin_mux);
	configure_module_pin_mux(mmc1_emmc_pin_mux);
	configure_module_pin_mux(usb_pin_mux);

	configure_module_pin_mux(i2c0_pin_mux);
	configure_module_pin_mux(i2c2_pin_mux);

	configure_module_pin_mux(uart3_pin_mux);
	configure_module_pin_mux(uart5_pin_mux);

	configure_module_pin_mux(unused_pin_mux);
}

void enable_uart0_pin_mux(void)
{
	configure_module_pin_mux(uart0_pin_mux);
}

void disable_uart0_pin_mux(void)
{
	configure_module_pin_mux(uart0_disabled_pin_mux);
}

void enable_uart1_pin_mux(void)
{
	configure_module_pin_mux(uart1_pin_mux);
}
