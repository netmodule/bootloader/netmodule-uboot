/*
 * (C) Copyright 2010
 * Texas Instruments, <www.ti.com>
 *
 * Aneesh V <aneesh@ti.com>
 *
 * SPDX-License-Identifier:	GPL-2.0+
 */
#include <common.h>
#include <dm.h>
#include <spl.h>
#include <linux/compiler.h>
#include <errno.h>
#include <asm/u-boot.h>
#include <errno.h>
#include <mmc.h>
#include <image.h>
#include <crc.h>

DECLARE_GLOBAL_DATA_PTR;


#define UBOOT_MIN_SIZE_BYTES	(128*1024)
#define UBOOT_MAX_SIZE_BYTES	(2500*1024)

#if defined(CONFIG_NM_BOOTLOADER_FORMAT)

static int image_nm_header_ok(const struct nm_header *header)
{
	u32 start_tag = __be32_to_cpu(header->nm_start_tag);
	u32 length = __be32_to_cpu(header->nm_length);

	if ((start_tag == 0x424c5354) &&
	    (length >= UBOOT_MIN_SIZE_BYTES) && (length <= UBOOT_MAX_SIZE_BYTES))
	{
		return 0;
	}
	else {
		return -EFAULT;
	}
}

static int mmc_load_netmodule(struct mmc *mmc, ulong sector,
			      const struct nm_header *header)
{
	u32 nm_header_size = sizeof(struct nm_header) -sizeof(struct image_header);
	const struct image_header *im_header;
	const __be32* end_tag_position;
	u32 image_size_sectors;
	unsigned long count; 
	u32 end_tag;
	int ret;

	im_header = &(header->header);

	/* decode header and fill out spl_image structure */
	ret = spl_parse_image_header(im_header);
	if (ret) {
		return ret;
	}

	/*
	 * convert size to sectors - round up,
	 * add 256 bytes for NetModule header, 4 bytes for NetModule trailer 
	 */
	image_size_sectors = (spl_image.size + nm_header_size + sizeof(u32) + 
			     mmc->read_bl_len - 1) /
			     mmc->read_bl_len;

	/*
	 * Read the header too to avoid extra memcpy,
	 * compensate the load address for the NetModule header 
	 */
	count = blk_dread(mmc_get_blk_desc(mmc), sector, image_size_sectors,
			  (void *)(ulong)spl_image.load_addr - nm_header_size);
	debug("read %x sectors to %x\n", image_size_sectors,
	      spl_image.load_addr);

	if (count != image_size_sectors) {
		puts(", read error");
		return -EIO;
	}

	/* Check NetModule end tag to be sure image is completely written */
	end_tag_position =  (const __be32*)(spl_image.load_addr + spl_image.size);
	end_tag = __be32_to_cpu(*end_tag_position);
	if (end_tag != 0x424c454e) {
		puts(", end marker fail");
		return -EFAULT;
	}

	puts(", end marker ok");

	return 0;
}

static int mmc_load_image_raw_sector_netmodule(struct mmc *mmc, unsigned long sector)
{
	unsigned long count;
	struct nm_header *header;
	int ret = 0;

	printf("Checking NM U-Boot at sector 0x%lx: ", sector);

	header = (struct nm_header *)(CONFIG_SYS_TEXT_BASE -
					 sizeof(struct nm_header));

	/* read image header to find the image size and load address */
	count = blk_dread(mmc_get_blk_desc(mmc), sector, 1, header);
	debug("hdr read sector %lx, count=%lu\n", sector, count);
	if (count == 0) {
		ret = -EIO;
		goto end;
	}

	if (image_nm_header_ok(header)) {
		puts("hdr fail");
		ret = -EFAULT;
		goto end;
	}

	puts("hdr ok");

	ret = mmc_load_netmodule(mmc, sector, header);

end:
	if (ret) {
		puts(" -> skipping\n");
		return -1;
	}

	puts("\n");

	return 0;
}

#endif /* CONFIG_NM_BOOTLOADER_FORMAT) */

static int mmc_load_legacy(struct mmc *mmc, ulong sector,
			   struct image_header *header)
{
	u32 header_size = sizeof(struct image_header);
	u32 image_size_sectors;
	u32 image_size_bytes;
	u32 data_crc;
	u32 data_crc_memory;
	unsigned long count;
	int ret;

	/*
	 * Note: spl_parse_image_header() always returns 0 
	 * Own header check required here.
	 */
	if (image_get_magic(header) != IH_MAGIC) {
		puts("hdr fail");
		return -EFAULT;
	}

	ret = spl_parse_image_header(header);
	if (ret)
		return ret;

	puts("hdr ok");

	/* TODO: This assumes a header is present (e.g. no PPC images) */
	/* Could also check spl_image.flags & SPL_COPY_PAYLOAD_ONLY above */

	image_size_bytes = __be32_to_cpu(header->ih_size);
	data_crc = __be32_to_cpu(header->ih_dcrc);

	if ((image_size_bytes < UBOOT_MIN_SIZE_BYTES) || (image_size_bytes > UBOOT_MAX_SIZE_BYTES)) {
		puts(", data len fail");
		return -EFAULT;
	}

	puts(", data len ok");

	/* convert size to sectors - round up */
	image_size_sectors = (spl_image.size + mmc->read_bl_len - 1) /
			     mmc->read_bl_len;

	/* Read the header too to avoid extra memcpy */
	count = blk_dread(mmc_get_blk_desc(mmc), sector, image_size_sectors,
			  (void *)(ulong)spl_image.load_addr);

	debug("read %x sectors to %x\n", image_size_sectors,
	      spl_image.load_addr);
	if (count != image_size_sectors) {
		puts(", read error");
		return -EIO;
	}

	/* Check data CRC */
	data_crc_memory = crc32(0, (const u8*)(spl_image.load_addr + header_size), image_size_bytes);
	if (data_crc_memory != data_crc) {
		puts(", data crc fail");
		return -EFAULT;
	}

	puts(", data crc ok");

	return 0;
}

static ulong h_spl_load_read(struct spl_load_info *load, ulong sector,
			     ulong count, void *buf)
{
	struct mmc *mmc = load->dev;

	return blk_dread(mmc_get_blk_desc(mmc), sector, count, buf);
}

static int mmc_load_image_raw_sector(struct mmc *mmc, unsigned long sector)
{
	unsigned long count;
	struct image_header *header;
	int ret = 0;

	printf("Checking U-Boot at sector 0x%lx: ", sector);

	header = (struct image_header *)(CONFIG_SYS_TEXT_BASE -
					 sizeof(struct image_header));

	/* read image header to find the image size & load address */
	count = blk_dread(mmc_get_blk_desc(mmc), sector, 1, header);
	debug("hdr read sector %lx, count=%lu\n", sector, count);
	if (count == 0) {
/*
#ifdef CONFIG_SPL_LIBCOMMON_SUPPORT
		puts("mmc_load_image_raw_sector: mmc block read error\n");
#endif
*/
		ret = -EIO;
		goto end;
	}

	if (IS_ENABLED(CONFIG_SPL_LOAD_FIT) &&
	    image_get_magic(header) == FDT_MAGIC) {
		struct spl_load_info load;

		debug("Found FIT\n");
		load.dev = mmc;
		load.priv = NULL;
		load.filename = NULL;
		load.bl_len = mmc->read_bl_len;
		load.read = h_spl_load_read;
		ret = spl_load_simple_fit(&load, sector, header);
	} else {
		ret = mmc_load_legacy(mmc, sector, header);
	}

end:
	if (ret) {
		puts(" -> skipping\n");
		return -1;
	}

	puts("\n");

	return 0;
}

int spl_mmc_get_device_index(u32 boot_device)
{
	switch (boot_device) {
	case BOOT_DEVICE_MMC1:
		return 0;
	case BOOT_DEVICE_MMC2:
	case BOOT_DEVICE_MMC2_2:
		return 1;
	}

#ifdef CONFIG_SPL_LIBCOMMON_SUPPORT
	printf("spl: unsupported mmc boot device.\n");
#endif

	return -ENODEV;
}

static int spl_mmc_find_device(struct mmc **mmcp, u32 boot_device)
{
#ifdef CONFIG_DM_MMC
	struct udevice *dev;
#endif
	int err, mmc_dev;

	mmc_dev = spl_mmc_get_device_index(boot_device);
	if (mmc_dev < 0)
		return mmc_dev;

	err = mmc_initialize(NULL);
	if (err) {
#ifdef CONFIG_SPL_LIBCOMMON_SUPPORT
		printf("spl: could not initialize mmc. error: %d\n", err);
#endif
		return err;
	}

#ifdef CONFIG_DM_MMC
	err = uclass_get_device(UCLASS_MMC, mmc_dev, &dev);
	if (!err)
		*mmcp = mmc_get_mmc_dev(dev);
#else
	*mmcp = find_mmc_device(mmc_dev);
	err = *mmcp ? 0 : -ENODEV;
#endif
	if (err) {
#ifdef CONFIG_SPL_LIBCOMMON_SUPPORT
		printf("spl: could not find mmc device. error: %d\n", err);
#endif
		return err;
	}

	return 0;
}

#ifdef CONFIG_SYS_MMCSD_RAW_MODE_U_BOOT_PARTITION
static int mmc_load_image_raw_partition(struct mmc *mmc, int partition)
{
	disk_partition_t info;
	int err;

	err = part_get_info(&mmc->block_dev, partition, &info);
	if (err) {
#ifdef CONFIG_SPL_LIBCOMMON_SUPPORT
		puts("spl: partition error\n");
#endif
		return -1;
	}

#ifdef CONFIG_SYS_MMCSD_RAW_MODE_U_BOOT_SECTOR
	return mmc_load_image_raw_sector(mmc, info.start +
					 CONFIG_SYS_MMCSD_RAW_MODE_U_BOOT_SECTOR);
#else
	return mmc_load_image_raw_sector(mmc, info.start);
#endif
}
#else
#define CONFIG_SYS_MMCSD_RAW_MODE_U_BOOT_PARTITION -1
static int mmc_load_image_raw_partition(struct mmc *mmc, int partition)
{
	return -ENOSYS;
}
#endif

#ifdef CONFIG_SPL_OS_BOOT
static int mmc_load_image_raw_os(struct mmc *mmc)
{
	unsigned long count;
	int ret;

	count = mmc->block_dev.block_read(&mmc->block_dev,
		CONFIG_SYS_MMCSD_RAW_MODE_ARGS_SECTOR,
		CONFIG_SYS_MMCSD_RAW_MODE_ARGS_SECTORS,
		(void *) CONFIG_SYS_SPL_ARGS_ADDR);
	if (count == 0) {
#ifdef CONFIG_SPL_LIBCOMMON_SUPPORT
		puts("mmc_load_image_raw_os: mmc block read error\n");
#endif
		return -1;
	}

	ret = mmc_load_image_raw_sector(mmc,
		CONFIG_SYS_MMCSD_RAW_MODE_KERNEL_SECTOR);
	if (ret)
		return ret;

	if (spl_image.os != IH_OS_LINUX) {
		puts("Expected Linux image is not found. Trying to start U-boot\n");
		return -ENOENT;
	}

	return 0;
}
#else
int spl_start_uboot(void)
{
	return 1;
}
static int mmc_load_image_raw_os(struct mmc *mmc)
{
	return -ENOSYS;
}
#endif

#ifdef CONFIG_SYS_MMCSD_FS_BOOT_PARTITION
int spl_mmc_do_fs_boot(struct mmc *mmc)
{
	int err = -ENOSYS;

#ifdef CONFIG_SPL_FAT_SUPPORT
	if (!spl_start_uboot()) {
		err = spl_load_image_fat_os(&mmc->block_dev,
			CONFIG_SYS_MMCSD_FS_BOOT_PARTITION);
		if (!err)
			return err;
	}
#ifdef CONFIG_SPL_FS_LOAD_PAYLOAD_NAME
	err = spl_load_image_fat(&mmc->block_dev,
				 CONFIG_SYS_MMCSD_FS_BOOT_PARTITION,
				 CONFIG_SPL_FS_LOAD_PAYLOAD_NAME);
	if (!err)
		return err;
#endif
#endif
#ifdef CONFIG_SPL_EXT_SUPPORT
	if (!spl_start_uboot()) {
		err = spl_load_image_ext_os(&mmc->block_dev,
			CONFIG_SYS_MMCSD_FS_BOOT_PARTITION);
		if (!err)
			return err;
	}
#ifdef CONFIG_SPL_FS_LOAD_PAYLOAD_NAME
	err = spl_load_image_ext(&mmc->block_dev,
				 CONFIG_SYS_MMCSD_FS_BOOT_PARTITION,
				 CONFIG_SPL_FS_LOAD_PAYLOAD_NAME);
	if (!err)
		return err;
#endif
#endif

#if defined(CONFIG_SPL_FAT_SUPPORT) || defined(CONFIG_SPL_EXT_SUPPORT)
	err = -ENOENT;
#endif

	return err;
}
#else
int spl_mmc_do_fs_boot(struct mmc *mmc)
{
	return -ENOSYS;
}
#endif

int spl_mmc_load_image(u32 boot_device)
{
	struct mmc *mmc = NULL;
	u32 boot_mode;
	int err = 0;
	__maybe_unused int part;

	err = spl_mmc_find_device(&mmc, boot_device);
	if (err)
		return err;

	err = mmc_init(mmc);
	if (err) {
#ifdef CONFIG_SPL_LIBCOMMON_SUPPORT
		printf("spl: mmc init failed with error: %d\n", err);
#endif
		return err;
	}

	boot_mode = spl_boot_mode();
	err = -EINVAL;
	switch (boot_mode) {
	case MMCSD_MODE_EMMCBOOT:
			/*
			 * We need to check what the partition is configured to.
			 * 1 and 2 match up to boot0 / boot1 and 7 is user data
			 * which is the first physical partition (0).
			 */
			part = (mmc->part_config >> 3) & PART_ACCESS_MASK;

			if (part == 7)
				part = 0;

			err = blk_dselect_hwpart(mmc_get_blk_desc(mmc), part);
			if (err) {
#ifdef CONFIG_SPL_LIBCOMMON_SUPPORT
				puts("spl: mmc partition switch failed\n");
#endif
				return err;
			}
			/* Fall through */
	case MMCSD_MODE_RAW:
		debug("spl: mmc boot mode: raw\n");

		if (!spl_start_uboot()) {
			err = mmc_load_image_raw_os(mmc);
			if (!err)
				return err;
		}

		err = mmc_load_image_raw_partition(mmc,
			CONFIG_SYS_MMCSD_RAW_MODE_U_BOOT_PARTITION);
		if (!err)
			return err;

#if defined(CONFIG_SYS_MMCSD_RAW_MODE_U_BOOT_SECTOR)

#if defined(CONFIG_NM_BOOTLOADER_FORMAT)
		/* Try to load NetModule packed bootloader from main location */
		err = mmc_load_image_raw_sector_netmodule(mmc,
			CONFIG_SYS_MMCSD_RAW_MODE_U_BOOT_SECTOR);
		if (!err)
			return err;	/* found -> ok */
#endif
		/* Try to load bootloader from main location */
		err = mmc_load_image_raw_sector(mmc,
			CONFIG_SYS_MMCSD_RAW_MODE_U_BOOT_SECTOR);
		if (!err)
			return err;	/* found -> ok */

#if defined(CONFIG_NM_BOOTLOADER_FORMAT)
		/* Try to load NetModule packed bootloader from alternate location */
		err = mmc_load_image_raw_sector_netmodule(mmc,
			CONFIG_SYS_MMCSD_RAW_MODE_U_BOOT_SECTOR_ALTERNATE);
		if (!err)
			return err;	/* found -> ok */
#endif

		/* Try to load regular bootloader from alternate location */
		err = mmc_load_image_raw_sector(mmc,
			CONFIG_SYS_MMCSD_RAW_MODE_U_BOOT_SECTOR_ALTERNATE);
		if (!err) {
			return err;	/* found -> ok */
		}
#endif
		/* If RAW mode fails, try FS mode. */
	case MMCSD_MODE_FS:
		debug("spl: mmc boot mode: fs\n");

		err = spl_mmc_do_fs_boot(mmc);
		if (!err)
			return err;

		break;
	case MMCSD_MODE_UNDEFINED:
#ifdef CONFIG_SPL_LIBCOMMON_SUPPORT
	default:
		puts("spl: mmc: wrong boot mode\n");
#endif
	}

	return err;
}
