/*
 * am335x_nrhw20.h
 *
 * Copyright (C) 2018-2019 NetModule AG - http://www.netmodule.com/
 * Copyright (C) 2011 Texas Instruments Incorporated - http://www.ti.com/
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation version 2.
 *
 * This program is distributed "as is" WITHOUT ANY WARRANTY of any
 * kind, whether express or implied; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

#ifndef __CONFIG_AM335X_NRHW20_H
#define __CONFIG_AM335X_NRHW20_H

#include <configs/ti_am335x_common.h>

/* Disable U-Boot load from filesystems, to save around 10 kB SPL image size */
#ifdef CONFIG_SYS_MMCSD_FS_BOOT_PARTITION
# undef CONFIG_SYS_MMCSD_FS_BOOT_PARTITION
#endif

#undef CONFIG_SPL_AM33XX_ENABLE_RTC32K_OSC

#undef CONFIG_HW_WATCHDOG
#undef CONFIG_OMPAP_WATCHDOG
#undef CONFIG_SPL_WATCHDOG_SUPPORT

#ifndef CONFIG_SPL_BUILD
# define CONFIG_TIMESTAMP
# define CONFIG_LZO
#endif

#define CONFIG_SYS_BOOTM_LEN		(16 << 20)

#define MACH_TYPE_TIAM335EVM		3589	/* Until the next sync */
#define CONFIG_MACH_TYPE		MACH_TYPE_TIAM335EVM
#define CONFIG_BOARD_LATE_INIT

/* TODO: It could be preconsole buffer is not properly working in SPL
 * Observed lock ups when printing too much text.
#define CONFIG_PRE_CONSOLE_BUFFER
#define CONFIG_PRE_CON_BUF_ADDR   	0x80000000
#define CONFIG_PRE_CON_BUF_SZ     	64*1024
*/

/* Clock Defines */
#define V_OSCK				0 /* 0 means detect from sysboot1 config */
#define V_SCLK				(V_OSCK)

#include <config_distro_bootcmd.h>

/* Dynamic override for PHY_ANEG_TIMEOUT value */
#if 0 /* TODO */
#ifndef CONFIG_SPL_BUILD
# ifndef __ASSEMBLER__
int eth_phy_timeout(void);
# endif
#endif
#define PHY_ANEG_TIMEOUT		eth_phy_timeout()
#endif
#define PHY_ANEG_DEFAULT_TIMEOUT	5000

#define CONFIG_ARP_TIMEOUT		200
#undef CONFIG_NET_RETRY_COUNT
#define CONFIG_NET_RETRY_COUNT		5
#define CONFIG_BOOTP_MAY_FAIL

#ifndef CONFIG_SPL_BUILD

/*
 * Memory map for booting Linux
 *
 * 0x80000000   32MB	KERNEL_ADDR (kernel_addr), kernel execution address
 * 0x82000000  190MB    KERNEL_ADDR_R (kernel_addr_r), FIT image/kernel loading address
 * 			 kernel will be relocated kernel_addr
 *                       for FIT images, ramdisc and dtb will be relocated to 
 *                       top of bootmemory (0x8e000000 downwards)
 * 0x8BE00000    1MB	FDT_ADDR_R (fdt_addr_r), device tree if separate from kernel/FIT
 * 0x8BF00000	 1MB	PXE_ADDR (pxefile_addr_r), pxe configuration file (pxe get command)
 * 0x8C000000   32MB	LOAD_ADDR (load_addr), loading address for generic files
 * <end of boot memory>
 * 0x8E000000    4B     NRSW reset reason
 *              32MB    <>, Free space
 * 0x90000000  256MB	<>, Free space, 512MB systems
 * 0xA0000000  512MB	<>, Free space, 1GB systems only
 * 0xC0000000		End of RAM
 */

#define KERNEL_ADDR	"0x80000000"
#define KERNEL_ADDR_R	"0x82000000"
#define FDT_ADDR_R	"0x8BE00000"
#define PXE_ADDR	"0x8BF00000"
#define LOAD_ADDR	"0x8C000000"

/*
 * Limit boot memory to 256 MBytes to comply with kernel initial memory layout
 * This is the official way to restrict image load addresses. 
 * Don't use xx_high_addr variables.
 */
#define BOOTM_SIZE      "0x0E000000"	

/* Set boot command depending of software environment */
#ifndef CONFIG_NRSW_BUILD
/* Yocto/OSTree boot command */
#define MAIN_BOOTCMD "run boot_ostree"
#else
/* NRSW boot command */
#define MAIN_BOOTCMD "run sdboot"
#endif

#define CONFIG_EXTRA_ENV_SETTINGS \
	/* Memory Adresses */ \
	"fdt_addr_r=" FDT_ADDR_R "\0" \
	"kernel_addr=" KERNEL_ADDR "\0" /* NRSW only */ \
	"kernel_addr_r=" KERNEL_ADDR_R  "\0" \
	"load_addr=" LOAD_ADDR "\0" \
	"pxefile_addr_r=" PXE_ADDR "\0" \
	"bootm_size=" BOOTM_SIZE "\0" \
	\
	/* Misc */ \
	"defaultconsole=ttyS1\0" \
	"fdt_skip_update=yes\0" \
	"bootdelay=0\0" \
	\
	/* Networking */ \
	"ethprime=cpsw\0" \
	"ethopts=ti_cpsw.rx_packet_max=1526\0" \
	"ipaddr=192.168.1.1\0" \
	"serverip=192.168.1.254\0" \
	"tftptimeout=2000\0" \
	"tftptimeoutcountmax=5\0" \
	"bootpretryperiod=10000\0" /* 2000 */ \
	"autoload=false\0" \
	\
	/* OSTree boot */ \
	"bootcmd_otenv=ext4load mmc 1:1 $load_addr /boot/loader/uEnv.txt; " \
			"setenv bootargs_prev $bootargs; " \
			"env import -t $load_addr $filesize; setenv bootargs $bootargs_prev $bootargs root=/dev/ram0 console=$defaultconsole,115200 " \
			"$ethopts rw ostree_root=/dev/mmcblk1p1\0" \
	"bootcmd_rd_in_mmc=ext4load mmc 1:1 $kernel_addr_r /boot$kernel_image; " \
			"bootm $kernel_addr_r\0" \
	"boot_ostree=run shieldcmd; run bootcmd_otenv; run bootcmd_rd_in_mmc\0" \
	\
	/* NRSW boot */ \
	"root_part=1\0" /* from NRSW, required here? set from board.c */ \
	"kernel_image=kernel.bin\0" \
	"fdt_image=am335x-nmhw24-prod1.dtb\0" /* diff, openwrt-nrhw24-nb801.dtb, not relevant as it will be overwritten */ \
	"add_sd_bootargs=setenv bootargs $bootargs root=/dev/${mmc_dev}p$root_part rootfstype=ext4 " \
			"console=$defaultconsole,115200 rootwait loglevel=4 ti_cpsw.rx_packet_max=1526\0" \
	"add_version_bootargs=setenv bootargs $bootargs\0" \
	"sdbringup=echo Try bringup boot && ext4load mmc 1:$root_part $kernel_addr /boot/zImage && " \
			"ext4load mmc 1:$root_part $fdt_addr /boot/$fdt_image && setenv bootargs $bootargs rw;\0" \
	"sdprod=ext4load mmc 1:$root_part $kernel_addr /boot/$kernel_image && " \
			"ext4load mmc 1:$root_part $fdt_addr /boot/$fdt_image && setenv bootargs $bootargs ro;\0" \
	"sdboot=env set fdt_addr " FDT_ADDR_R "; "\
			"if mmc dev 1; then echo Copying Linux from SD to RAM...; "\
			"if test -e mmc 1:$root_part /boot/$kernel_image; then run sdprod; " \
			"else run sdbringup; fi; " \
			/* For v4.19 kernel $mmc_dev should be "mmcblk1" (read from DT), for v3.18 kernel: "mmcblk0" */ \
			"fdt addr $fdt_addr;if fdt get value mmc_dev /nm_env nm,mmc-dev;then;else setenv mmc_dev mmcblk0;fi;" \
			"run add_sd_bootargs; run add_version_bootargs; run shieldcmd; " \
			"bootz $kernel_addr - $fdt_addr; fi\0" \
	\
	/* Boot command */ \
	"bootcmd=" MAIN_BOOTCMD "\0" \
	\
	/* Recovery boot (same for OSTree and NRSW) */ \
	"recovery=run pxe_recovery || setenv ipaddr $ipaddr; setenv serverip $serverip; run tftp_recovery\0" \
	/* setenv ipaddr and serverip is necessary, because dhclient destroys the IPs internally */ \
	"pxe_recovery=mdio up $ethprime && dhcp && pxe get && pxe boot\0" \
	"tftp_recovery=tftpboot $kernel_addr_r recovery-image; tftpboot $fdt_addr_r recovery-dtb; " \
			"setenv bootargs rdinit=/etc/preinit console=$defaultconsole,115200 " \
			"debug $ethopts; " \
			"bootz $kernel_addr_r - $fdt_addr_r\0" /* kernel_addr_r */

#endif


/* TODO: Check if ok for NRSW? */
#define CONFIG_ZERO_BOOTDELAY_CHECK

/* UART Configuration */
#define CONFIG_SYS_NS16550_COM1			0x44e09000	/* UART0: XModem Boot, Shield */
#define CONFIG_SYS_NS16550_COM2			0x48022000	/* UART1: eMMC Boot, User UART */
/* TODO: Can the following be removed, as they shouldn't be required for bootloader */
#define CONFIG_SYS_NS16550_COM3			0x48024000	/* Unused */
#define CONFIG_SYS_NS16550_COM4			0x481A6000	/* GNSS */
#define CONFIG_SYS_NS16550_COM5			0x481A8000	/* Unused */
#define CONFIG_SYS_NS16550_COM6			0x481AA000	/* Bluetooth */

#define CONFIG_I2C
#define CONFIG_I2C_MULTI_BUS

#define CONFIG_CMD_EEPROM
#define CONFIG_SYS_I2C_EEPROM_ADDR              0x50	/* Main EEPROM */
#define CONFIG_SYS_I2C_EEPROM_ADDR_LEN          2
#define CONFIG_SYS_I2C_SPEED                    100000
#define CONFIG_SYS_EEPROM_PAGE_WRITE_BITS       4
#define CONFIG_SYS_EEPROM_PAGE_WRITE_DELAY_MS   50	/* TODO: Can this be reduced to 20ms */

#define CONFIG_PCA953X
#define CONFIG_SYS_I2C_PCA953X_BUS		2
#define CONFIG_SYS_I2C_PCA953X_ADDR		0x74
#define CONFIG_SYS_I2C_PCA953X_WIDTH		{ {0x74, 16} }
#define CONFIG_CMD_PCA953X
#define CONFIG_CMD_PCA953X_INFO


/* Put Environment in eMMC */
#define CONFIG_ENV_OFFSET			(512 * 128)	/* @ 512*256 SPL starts */
#define CONFIG_ENV_SIZE				(4 * 1024)
#define CONFIG_ENV_IS_IN_MMC
#define CONFIG_SYS_MMC_ENV_DEV			1

#undef CONFIG_SPL_ENV_SUPPORT
#undef CONFIG_SPL_NAND_SUPPORT
#undef CONFIG_SPL_ONENAND_SUPPORT

/* We need to disable SPI to not confuse the eeprom env driver */
#undef CONFIG_SPI
#undef CONFIG_SPI_BOOT
#undef CONFIG_SPL_OS_BOOT

#define CONFIG_SPL_YMODEM_SUPPORT

#define CONFIG_SPL_LDSCRIPT		"$(CPUDIR)/am33xx/u-boot-spl.lds"

#define CONFIG_SUPPORT_EMMC_BOOT

/*
 * USB configuration.  We enable MUSB support, both for host and for
 * gadget.  We set USB0 as peripheral and USB1 as host, based on the
 * board schematic and physical port wired to each.  Then for host we
 * add mass storage support and for gadget we add both RNDIS ethernet
 * and DFU.
 */
#define CONFIG_USB_MUSB_DSPS
#define CONFIG_ARCH_MISC_INIT
#define CONFIG_USB_MUSB_PIO_ONLY
#define CONFIG_USB_MUSB_DISABLE_BULK_COMBINE_SPLIT
#define CONFIG_AM335X_USB0
#define CONFIG_AM335X_USB0_MODE		MUSB_HOST

/* To support eMMC booting */
#define CONFIG_STORAGE_EMMC
#define CONFIG_FASTBOOT_FLASH_MMC_DEV   1

#ifdef CONFIG_USB_MUSB_HOST
#define CONFIG_USB_STORAGE
#endif

#ifdef CONFIG_USB_MUSB_GADGET
/* Removing USB gadget and can be enabled adter adding support usb DM */
#ifndef CONFIG_DM_ETH
#define CONFIG_USB_ETHER
#define CONFIG_USB_ETH_RNDIS
#define CONFIG_USBNET_HOST_ADDR	"de:ad:be:af:00:00"
#endif /* CONFIG_DM_ETH */
#endif /* CONFIG_USB_MUSB_GADGET */


/*
 * Disable MMC DM for SPL build and can be re-enabled after adding
 * DM support in SPL
 */
#ifdef CONFIG_SPL_BUILD
#undef CONFIG_DM_MMC
#undef CONFIG_TIMER
#endif

#if defined(CONFIG_SPL_BUILD)
/* Remove other SPL modes. */
#undef CONFIG_SPL_NAND_SUPPORT
#define CONFIG_ENV_IS_NOWHERE
#undef CONFIG_PARTITION_UUIDS
#undef CONFIG_EFI_PARTITION
#endif

/* Network. */
#define CONFIG_IP_DEFRAG		/* so we can use large tftp blocks */
#define CONFIG_TFTP_TSIZE		/* tftp transfer size, progress bar */

#define CONFIG_PHYLIB

/* Ethernet Switch Support */
#define CONFIG_MV88E60XX_SWITCH
#define CONFIG_MV88E60XX_PHY_PORTS	0x0F
#define CONFIG_MV88E60XX_CPU_PORT	5


#define CONFIG_CMD_MEMTEST
#define CONFIG_SYS_MEMTEST_START    0x84000000
#define CONFIG_SYS_MEMTEST_END      0x87900000


#ifdef CONFIG_NRSW_BUILD
/* support for NM packed bootloader */
#define CONFIG_NM_BOOTLOADER_FORMAT

/* password protected login */
#define CONFIG_CRYPT
#define CONFIG_NM_LOGIN
#define CONFIG_NM_LOGIN_PART	"1:3"	/* TODO: Define location of file for OSTree/Yocto */
#define CONFIG_NM_LOGIN_PASSWD	"/root/boot/bootpass"
#endif

#define CONFIG_CMD_PXE

#define CONFIG_OF_BOARD_SETUP

#define CONFIG_JTAG_MARKER_SPL		0x402FFF00
#define CONFIG_JTAG_MARKER_UBOOT	0x807FFF00

/* NRSW PMIC Reset Reason */
#ifdef CONFIG_NRSW_BUILD
#define RESET_REASON_SHM_LOCATION	0x8e000000
#define EXTERNAL_WATCHDOG_PATTERN	0x781f9ce2
#endif


/* SPL command is not needed */
#undef CONFIG_CMD_SPL

/* Never enable ISO it is broken and can lead to a crash */
#undef CONFIG_ISO_PARTITION

#endif	/* ! __CONFIG_AM335X_NRHW20_H */
