#ifndef _CRYPT_H
#define _CRYPT_H

extern char * sha_crypt(const char *key, const char *salt);

#endif /* _CRYPT_H */
